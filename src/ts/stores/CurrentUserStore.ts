/**
* CurrentUserStore.tsx
* Copyright: Microsoft 2018
*
* Singleton store that maintains information about the currently-signed-in user.
*/

import { autoSubscribe, AutoSubscribeStore, StoreBase } from 'resub';

import { User } from '../models/IdentityModels';

@AutoSubscribeStore
export class CurrentUserStore extends StoreBase {
    // TODO - properly initialize
    private _user: User = {     
        id: 0,
        firstname: 'string',
        lastname: 'string',
        username:'string',
        email:'string',
        profile_pic:'string',
        modo:'string',
        groups:{
            1:'string',
            2:'string',
            3:'string'
        },
        goals:{
            fibra:'string',
            calorias:'string',
            grasas:'string',
            proteinas:'string',
            carbohidratos:'string',
            role:0
        }
    };

    @autoSubscribe
    getUser():User {
        return this._user
    }

    
    setUser(user:User) {
        this._user=user
        this.trigger()
    }
    @autoSubscribe
    getFullName(): string {
        return this._user ? this._user.firstname : '';
    }
}

export default new CurrentUserStore();
