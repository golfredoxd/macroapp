/**
* TodosStore.tsx
* Copyright: Microsoft 2017
*
* Resub Basic Example https://github.com/Microsoft/ReSub
*/

import * as _ from 'lodash';
import { autoSubscribe, AutoSubscribeStore, StoreBase,autoSubscribeWithKey } from 'resub';

import LocalDb from '../app/LocalDb';
import { Todo,Goal1,Goal2,Group,Food,Todo2 } from '../models/TodoModels';
import { DateObject } from 'react-native-calendars';

export enum TriggerKeys {
    ResponsiveWidth,
    Width,
    getDate,
    de,
    al,
    ce
}

@AutoSubscribeStore
class TodosStore extends StoreBase {
    private _todos: Todo[] = [];
    private _todos2: Todo2[] = [];
    private _todosDe: Todo2[] = [];
    private _todosAl: Todo2[] = [];
    private _todosCe: Todo2[] = [];
    token : string='';
    group : number=0;
    user_id:string=''
    goal1:Goal1={
        fibra:0,
        calorias:0,
        grasas:0,
        proteinas:0,
        carbohidratos:0,
    };
    goal2:Goal2={
        fibra:1,
        grasas:1,
        proteinas:1,
        total:1,
        carbohidratos:1,
    };
     groupFoods:Group={
        1:'',
        2:'',
        3:'',
    };
 todo:DateObject=this.getCurrentDate();

 startup() {
    return LocalDb.getAllTodos().then(todos => {
        this._todos = todos;
    });
}
addToken(object: Todo) {
    const newTodo: Todo = object;


    // Asynchronously write the new todo item to the DB.

    this.trigger();

    return newTodo;
}
  @autoSubscribe
    setTodos(todos:Todo[]) {
        this._todos=[]
      this._todos=todos.map((todo, i) => (
          {   
                    
            id:todo?.id===undefined?'default':todo.id.toString(),
            food_pic:todo?.food_pic===undefined?'default':todo.food_pic,
            nombre:todo?.nombre===undefined?'default':todo.nombre,
            marca:todo?.marca===undefined?'default':todo.marca,
            descripcion:todo?.descripcion===undefined?'default':todo.descripcion,
            unidad:todo?.unidad===undefined?'default':todo.unidad,
            cantidad:todo?.cantidad===undefined?0:todo.cantidad,
            fibra:todo?.fibra===undefined?0:todo.fibra,
            proteinas:todo?.proteinas===undefined?0:todo.proteinas,
            carbohidratos:todo?.carbohidratos===undefined?0:todo.carbohidratos,
            grasas:todo?.grasas===undefined?0:todo.grasas,
            calorias:todo?.calorias===undefined?0:todo.calorias,
            sodio:todo?.sodio===undefined?0:todo.sodio,
            public:todo?.public===undefined?0:todo.public,
            comentario:todo?.comentario===undefined?'default':todo.food_pic,
            user_id:todo?.user_id===undefined?0:todo.user_id,
                        }));
    }

    @autoSubscribe
    setTodos2(todos:Todo2[]) {
        this._todosAl=[]
        this._todosCe=[]
        this._todosDe=[]
        this._todos2=[]
        
      this._todos2=todos.map((todo, i) => (
          {   
            id:todo?.id===undefined?0:todo.id,
            user_id:todo?.user_id===undefined?0:todo.user_id,
            food_id:todo?.food_id===undefined?0:todo.food_id,
            group_id:todo?.group_id===undefined?0:todo.group_id,
            is_goal:todo?.is_goal===undefined?'':todo.is_goal,
            cantidad:todo?.cantidad===undefined?0:todo.cantidad,
            carbohidratos:todo?.carbohidratos===undefined?0:todo.carbohidratos,
            proteinas:todo?.proteinas===undefined?0:todo.proteinas,
            grasas:todo?.grasas===undefined?0:todo.grasas,
            fibra:todo?.fibra===undefined?0:todo.fibra,
            total:todo?.total===undefined?0:todo.total,
            fecha:todo?.fecha===undefined?'':todo.fecha,
            comentario:todo?.comentario===undefined?'':todo.comentario,
            food:{
              id:todo?.food.id===undefined?0:todo.food.id,
              food_pic:todo?.food.food_pic===undefined?'':todo.food.food_pic,
              nombre:todo?.food.nombre===undefined?'':todo.food.nombre,
              marca:todo?.food.marca===undefined?'':todo.food.marca,
              descripcion:todo?.food.descripcion===undefined?'':todo.food.descripcion,
              unidad:todo?.food.unidad===undefined?'':todo.food.unidad,
              cantidad_porcion:todo?.food.cantidad_porcion===undefined?0:todo.food.cantidad_porcion,
              fibra:todo?.food.fibra===undefined?0:todo.food.fibra,
              proteinas:todo?.food.proteinas===undefined?0:todo.food.proteinas,
              carbohidratos:todo?.food.carbohidratos===undefined?0:todo.food.carbohidratos,
              grasas:todo?.food.grasas===undefined?0:todo.food.grasas,
              calorias:todo?.food.calorias===undefined?0:todo.food.calorias,
              sodio:todo?.food.sodio===undefined?0:todo.food.sodio,
              public:todo?.food.public===undefined?0:todo.food.public,
              comentario:todo?.food.comentario===undefined?'':todo.food.comentario,
              user_id:todo?.food.user_id===undefined?0:todo.food.user_id,
            }

                        }));
                        this._todos2.map((todo,i)=>{
                            if(todo?.group_id===1){
                                this._todosDe=this._todosDe.concat(todo)
                            } else if(todo?.group_id===2){
                                
                                this._todosAl=this._todosAl.concat(todo)
                            } else if(todo?.group_id===3){
                                
                                this._todosCe=this._todosCe.concat(todo)
                            }
                        })
                        this.trigger(TriggerKeys.de)
                        this.trigger(TriggerKeys.al)
                        this.trigger(TriggerKeys.ce)
                        this.trigger()
    }
    addTodo2(object: Todo2) {
        const newTodo: Todo2 = object;

        this._todos2 = this._todos2.concat(newTodo);

        // Asynchronously write the new todo item to the DB.

        this.trigger();

        return newTodo;
    }

    addTodo(object: Todo) {
        const newTodo: Todo = object;

        this._todos = this._todos.concat(newTodo);

        // Asynchronously write the new todo item to the DB.
        LocalDb.putTodo(newTodo);

        this.trigger();

        return newTodo;
    }


    setGroup(group: number) {
     this.group=group;

        this.trigger();

    }
    @autoSubscribe
    getFoodGroup(): Group  {
        return this.groupFoods;
    }

    @autoSubscribe
    getUserId(): string  {
        return this.user_id;
    }

    @autoSubscribe
    getGroup(): number  {
        return this.group;
    }
    @autoSubscribe
    getGoal1(): Goal1  {
        return this.goal1;
    }
    @autoSubscribe
    getGoal2(): Goal2  {
        return this.goal2;
    }
    @autoSubscribe
    getToken(): string  {
        return this.token;
    }
    setFood(list: Food[]) {
        this.trigger();

    
    }
    setUserId(group: string) {
        this.user_id=group;
        this.trigger();

    
    }
    setToken(object: string) {
        this.token=object
        this.trigger();

    
    }
    setGoal(goal: Goal1) {
        this.goal1=goal;
        this.trigger();

    
    }

    setGroups(group: Group) {
        this.groupFoods=group;
        this.trigger();

    
    }
    setGoal2(goal: Goal2) {
        this.goal2=goal;
        this.trigger();

    
    }
    setDate() {
        this.todo=this.getCurrentDate();
        this.trigger();

    
    }
    setTodoDate(current: DateObject) {
      this.todo=current
      this.trigger(TriggerKeys.getDate);
    }
    @autoSubscribeWithKey(TriggerKeys.getDate)
    getDate3(): string  {
        var today = this.todo.year + '/' + this.todo.month  + '/' +  this.todo.day;
        return today;
    }

    @autoSubscribeWithKey(TriggerKeys.getDate)
    getDate2(): string  {
        var today = this.todo.year + '-' + this.todo.month  + '-' +  this.todo.day;
        return today;
    }
    @autoSubscribeWithKey(TriggerKeys.getDate)
    getDate(): string  {
        var today = this.todo.year + '/' + this.todo.month  + '/' +  this.todo.day;
        return today;
    }

    @autoSubscribe
    getDateToday() {  
         var today = new Date();
        var dd = String(today. getDate()). padStart(2, '0');
        var mm = String(today. getMonth() + 1). padStart(2, '0'); //January is 0!
        var yyyy = today. getFullYear();
        var today2 = yyyy + '-' + mm  + '-' +  dd;

        return today2
     
    }
    @autoSubscribe
    getCurrentDate() {  
         var today = new Date();
        var dd = String(today. getDate()). padStart(2, '0');
        var mm = String(today. getMonth() + 1). padStart(2, '0'); //January is 0!
        var yyyy = today. getFullYear();
        ​var result={
            day:parseInt(dd),
            month:parseInt(mm),
            year:yyyy,
            dateString:'',
            timestamp:0,
        };
        return result
     
    }

    @autoSubscribeWithKey(TriggerKeys.de)
    getTodosDe() {
        return this._todosDe;
    }

    @autoSubscribeWithKey(TriggerKeys.al)
    getTodosAl() {
        return this._todosAl;
    }

    @autoSubscribeWithKey(TriggerKeys.ce)
    getTodosCe() {
        return this._todosCe;
    }


    @autoSubscribe
    getTodos2() {
        return this._todos2;
    }

    @autoSubscribe
    getTodos() {
        return this._todos;
    }

    @autoSubscribe
    getTodoById(todoId: string) {
        return _.find(this._todos, todo => todo.id === todoId);
    }


    deleteTodo(todoId: string) {
        //this._todos = _.filter(this._todos, todo => todo.id !== todoId);

        // Asynchronously delete the todo item from the DB.
        LocalDb.deleteTodo(todoId);
        this.trigger();
    }
}

export default new TodosStore();
