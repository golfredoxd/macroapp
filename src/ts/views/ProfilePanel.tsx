/**
* CreateTodoPanel.tsx
* Copyright: Microsoft 2017
*
* The Todo item edit view.
*/

import * as RX from 'reactxp';


import ResponsiveWidthStore from '../stores/ResponsiveWidthStore';

import { GenericRestClient,ApiCallOptions } from 'simplerestclients';
import TodosStore from '../stores/TodosStore';
interface CreateTodoPanelProps extends RX.CommonProps {

}


import { FontSizes,Colors } from '../app/Styles';
import AppConfig from '../app/AppConfig';
interface Style {
  backgroundColor:string;
}
interface Entries {
  img:string;
  imgText:string;
  title:string;
  style:Style;
  content:string;
}
interface CreateTodoPanelState {
    width: number;
    calorias:number;
    fibra:number;
    carbohidratos:number;
    proteinas:number;
    grasas:number;
    activeSlide: number;
    refresh:boolean;
    height:number;
    entries:Entries[]
    item:any;
    user:User;
    background:string;
}
const _styles = {
    container: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        padding: 16
    }),
    slider: RX.Styles.createViewStyle({
      overflow:'hidden'
    }),
    slide: RX.Styles.createViewStyle({

      }),
    title: RX.Styles.createTextStyle({
      color: "black"
    }),
    exampleContainer: RX.Styles.createViewStyle({
      flexDirection:'column', 

    }),
    exampleContainerDark: RX.Styles.createViewStyle({
      overflow:'hidden',
    }),
    sliderContentContainer: RX.Styles.createViewStyle({
      overflow:'hidden',
    }),
    
    TextRegular: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        color: 'black',
    }),
    TextBold: RX.Styles.createTextStyle({
        font: Fonts.displayBold,
        color: '#51A33E',
    }),
    TextSemi: RX.Styles.createTextStyle({
        font: Fonts.displaySemibold,
        color: 'black',
    }),
    TextLight: RX.Styles.createTextStyle({
        font: Fonts.displayLight,
        color: 'black',
    }),
    buttonContainer: RX.Styles.createViewStyle({
        margin: 8,
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }),
    searchBox: RX.Styles.createTextInputStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.size14,
        borderWidth: 1,
        borderColor: Colors.borderSeparator,
        flex: 1,
        padding: 4,
        height:30,
        marginBottom:10,
        width:RX.UserInterface.measureWindow().width*0.6,
        marginLeft: 12,
    }),
    todoText: RX.Styles.createTextStyle({
        margin: 5,
        font:Fonts.displayRegular,
        fontSize: 14,
        alignSelf: 'stretch',
        backgroundColor: 'transparent',
        textAlign:'left'
    }),
    todoText2: RX.Styles.createTextStyle({
        margin: 5,
        font:Fonts.displayBold,
        fontSize: 16,
        alignSelf: 'stretch',
        backgroundColor: 'transparent',
        textAlign:'left'
    }),
    todoText3: RX.Styles.createTextStyle({
        margin: 8,
        font:Fonts.displayBold,
        fontSize: 18,
        alignSelf: 'stretch',
        backgroundColor: 'transparent',
        textAlign:'center'
    }),
    TextBtn: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        color: '#FFFFFF',
    }),
};

import {  Fonts, } from '../app/Styles';


import { User } from '../models/IdentityModels';
  import { ComponentBase } from 'resub';
import  CurrentUserStore  from '../stores/CurrentUserStore';
import NavContextStore from '../stores/NavContextStore';

export default class ProfilePanel extends ComponentBase<CreateTodoPanelProps, CreateTodoPanelState> {
  _background:string='white'
    protected _buildState(props: CreateTodoPanelProps, initState: boolean): Partial<CreateTodoPanelState>| undefined {
     
      
      
      let partialState: Partial<CreateTodoPanelState> = {
           width:ResponsiveWidthStore.getWidth(),
           user:CurrentUserStore.getUser(),
      }
        return partialState;
    }
   
    render() {
    return (
      <RX.View style={[_styles.exampleContainer, _styles.exampleContainerDark,{flex:1,backgroundColor:'white',width:this.state.width}]}>
  <RX.ScrollView>
  <RX.View style={{flexDirection:'row',flex:50}}>
  <RX.View style={{justifyContent:'center',alignItems:'center',flex:50}}>
  <RX.Image style={{height:100,width:100, alignSelf:'center'}} source={this.state.user.profile_pic}/>
</RX.View> 
<RX.View style={{justifyContent:'center',alignItems:'flex-start',marginTop:20,marginBottom:20,flex:50}}>
 
<RX.Text style={ _styles.todoText }>
    {'Nombre: '}
</RX.Text> 
                <RX.Text style={ _styles.todoText2 }>
                    { this.state.user ? this.state.user.firstname : '' }
                </RX.Text>
    
      <RX.Text style={ _styles.todoText }>
          {'Apellido: '}
      </RX.Text> 
                      <RX.Text style={ _styles.todoText2 }>
                          { this.state.user ? this.state.user.lastname : '' }
                      </RX.Text>
          
           
<RX.Text style={ _styles.todoText }>
    {'Correo: '}
</RX.Text> 
                <RX.Text style={ _styles.todoText2 }>
                    { this.state.user ? this.state.user.email : '' }
                </RX.Text>
                </RX.View>
                </RX.View>
                <RX.View style={{flex:50,justifyContent:'center',alignItems:'center'}}>
       
<RX.Text style={ _styles.todoText3 }>
    {'Nuevas Metas: '}
</RX.Text> 
       <RX.TextInput
                        style={ _styles.searchBox }
                        value={ this.state.calorias?.toString() }
                        autoFocus={ !AppConfig.isTouchInterface() }
                        placeholder={ 'Calorias' }
                        onChangeText={ this._onChangeTextSearchCalorias }
                        autoCapitalize={ 'none' }
                    />
                    
       
       <RX.TextInput
                        style={ _styles.searchBox }
                        value={ this.state.fibra?.toString() }
                        autoFocus={ !AppConfig.isTouchInterface() }
                        placeholder={ 'Fibra' }
                        onChangeText={ this._onChangeTextSearchFibra }
                        autoCapitalize={ 'none' }
                    />
                    <RX.TextInput
                                     style={ _styles.searchBox }
                                     value={ this.state.carbohidratos?.toString() }
                                     autoFocus={ !AppConfig.isTouchInterface() }
                                     placeholder={ 'Carbohidratos' }
                                     onChangeText={ this._onChangeTextSearchCarbohidratos }
                                     autoCapitalize={ 'none' }
                                 />
                         
                    <RX.TextInput
                                     style={ _styles.searchBox }
                                     value={ this.state.proteinas?.toString() }
                                     autoFocus={ !AppConfig.isTouchInterface() }
                                     placeholder={ 'Proteinas' }
                                     onChangeText={ this._onChangeTextSearchProteinas }
                                     autoCapitalize={ 'none' }
                                 />        
                         
                                 <RX.TextInput
                                                  style={ _styles.searchBox }
                                                  value={ this.state.grasas?.toString() }
                                                  autoFocus={ !AppConfig.isTouchInterface() }
                                                  placeholder={ 'Grasas' }
                                                  onChangeText={ this._onChangeTextSearchGrasas }
                                                  autoCapitalize={ 'none' }
                                              />    
                                               </RX.View>    
       <RX.View style={ _styles.buttonContainer }>
           
       <RX.Button onPress={ this._onPressDelete }  style={{borderRadius:40,marginBottom:20,backgroundColor:'#4e58ca',alignSelf:'center',alignItems:'center',justifyContent:'center',width:this.state.width*0.8, height:50}}>
    <RX.Text style={_styles.TextBtn}> {'GUARDAR METAS'}</RX.Text> 
            </RX.Button>
                    </RX.View>
                    </RX.ScrollView>
      </RX.View>
    );
    }
    private _onChangeTextSearchGrasas = (newValue: string) => {
        this.setState({
            grasas: parseFloat(newValue),
        })
                  
    };
    private _onChangeTextSearchProteinas = (newValue: string) => {
        this.setState({
            proteinas: parseFloat(newValue),
        })
                  
    };
    private _onChangeTextSearchCarbohidratos = (newValue: string) => {
        this.setState({
            carbohidratos: parseFloat(newValue),
        })
                  
    };
    
    private _onChangeTextSearchFibra = (newValue: string) => {
        this.setState({
            fibra: parseFloat(newValue),
        })
                  
    };
    
    private _onChangeTextSearchCalorias = (newValue: string) => {
        this.setState({
            calorias: parseFloat(newValue),
        })
                  
    };
    
    private _onPressDelete = (e: RX.Types.SyntheticEvent) => {
            
           

      const headers = {
      'Authorization': `Bearer ${TodosStore.getToken()}`,
      };

      class Http extends GenericRestClient {
      protected _getHeaders(options: ApiCallOptions): { [header: string]: string } {
          return headers;
      }
      }

      const http = new Http('http://ezeros.com:3001/api');
          http.performApiPost(`/user/goal`, {carbohidratos:this.state.carbohidratos,grasas:this.state.grasas,fibra:this.state.fibra,proteinas:this.state.proteinas,calorias:this.state.calorias})
          .then((resp:any)=>{                  

            http.performApiGet(`/diary/TotalbyDate/${TodosStore.getUserId()}/${TodosStore.getDateToday()}`,{ contentType: 'json' }).then((respuesta5:any )=> {
              let goal= {
                total:respuesta5.body.total,
                fibra:respuesta5.body.fibra,
                
                
                grasas:respuesta5.body.grasas,
                
                proteinas:respuesta5.body.proteinas,
                
                carbohidratos:respuesta5.body.carbohidratos,
                
              }
              TodosStore.setGoal2(goal)
              
http.performApiGet(`/user/goal/${TodosStore.getUserId()}`,{ contentType: 'json' }).then((respuesta:any )=> {
  let goal= {
    fibra:parseFloat(respuesta.body.fibra),
    
    calorias:parseFloat(respuesta.body.calorias),
    
    grasas:parseFloat(respuesta.body.grasas),
    
    proteinas:parseFloat(respuesta.body.proteinas),
    
    carbohidratos:parseFloat(respuesta.body.carbohidratos),
    
  }
  
  TodosStore.setGoal(goal);
  
  NavContextStore.navigateToTodoList();
          });
                              });
                              
                           

      });
      
}
}
