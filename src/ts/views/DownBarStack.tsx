/*
* TopBarStack.tsx
* Copyright: Microsoft 2018
*
* Horizontal bar that appears on the top of every view within the app
* when it's using stack-based layout.
*/

import * as RX from 'reactxp';
import { ComponentBase } from 'resub';
import { Colors, Fonts, FontSizes, Styles } from '../app/Styles';
import ResponsiveWidthStore from '../stores/ResponsiveWidthStore';

const _styles = {
    background: RX.Styles.createViewStyle({
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        borderTopWidth:1,
        bottom:0,
        position:'absolute',
        flex:1,
    
    }),
    leftRightContainer: RX.Styles.createViewStyle({
        flexDirection: 'row',
        alignItems: 'center',
        width: 120,
        marginRight:20,
    }),
    RightContainer: RX.Styles.createViewStyle({
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent:'flex-end',
        width: RX.UserInterface.measureWindow().width*0.40,        
        marginRight:ResponsiveWidthStore.getWidth()*0.08
    }),
    titleContainer: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
        flexDirection:'row',
        alignItems:'center',
    }),
    titleText: RX.Styles.createTextStyle({
        flex: -1,
        font: Fonts.displayRobotoBold,
        fontSize: 14,
        width:200,
        color: 'black',
        textAlign: 'center'
    }),
    titleText2: RX.Styles.createTextStyle({
        flex: -1,
        font: Fonts.displayRegular,
        fontSize: 12,
        width:200,
        color: 'black',
        textAlign: 'center'
    }),
    titleText4: RX.Styles.createTextStyle({
        flex: -1,
        font: Fonts.displayBold,
        fontSize: 24,
        color: 'black',
        textAlign: 'center'
    }),
    titleText3: RX.Styles.createTextStyle({
        flex: -1,
        font: Fonts.displayBold,
        fontSize: 20,
        color: 'black',
        textAlign: 'center'
    }),
    backText: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.size16,
        color: Colors.menuText,
        margin: 8
    }),
    backTextHover: RX.Styles.createTextStyle({
        color: Colors.menuTextHover
    })
};

import ImageSource from 'modules/images';
import TodosStore from '../stores/TodosStore';
import AccountMenuButton from './AccountMenuButton';
import AlertMenuButton from './AlertMenuButton';
import NavContextStore from '../stores/NavContextStore';
export interface TopBarStackProps extends RX.CommonProps {
    title: string;
}

interface CreateTodoPanelState {
    width: number;
    currentDate:string;
    height:number;
}
export default class DownBarStack extends ComponentBase<TopBarStackProps, CreateTodoPanelState> {
    protected _buildState(props: TopBarStackProps, initState: boolean): Partial<CreateTodoPanelState> | undefined {
        let partialState: Partial<CreateTodoPanelState> = {
           width:ResponsiveWidthStore.getWidth(),
           height:ResponsiveWidthStore.getHeight(),
           currentDate:TodosStore.getDate(),
           };
        return partialState;
    }
    render(): JSX.Element | null {
       

        return (
            <RX.View style={ [_styles.background, Styles.statusBarTopMargin,{
                backgroundColor:'white',width:this.state.width,height:this.state.height*0.1,justifyContent:'center',alignItems:'center'}] }>
               
               <RX.View style={{flex:33,justifyContent:'center',alignItems:'center',height:20,width:20}}>
                  <AccountMenuButton/>
                   </RX.View>
              
                   
                   <RX.View style={{flex:33,justifyContent:'center',alignItems:'center'}}>
                 <RX.Button onPress={this._onPress}>
                  <RX.Image source={ImageSource.home} style={{height:25,width:25}}/>
                  </RX.Button>   
                      </RX.View>
                   
                   <RX.View style={{flex:33,justifyContent:'center',alignItems:'center',height:20,width:20}}>
                  <AlertMenuButton/>
                   </RX.View>
            </RX.View>
        );
    }

    _onPress=()=>{
        NavContextStore.navigateToTodoList()
    }
}
