/**
* CreateTodoPanel.tsx
* Copyright: Microsoft 2017
*
* The Todo item edit view.
*/

import * as RX from 'reactxp';

import ResponsiveWidthStore from '../stores/ResponsiveWidthStore';


interface CreateTodoPanelProps extends RX.CommonProps {

}

interface CreateTodoPanelState {
    width: number;
    height:number;
}
const _styles = {
    container: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        padding: 16
    }),
    slider: RX.Styles.createViewStyle({
      overflow:'hidden'
    }),
    slide: RX.Styles.createViewStyle({

      }),
    todoImage: RX.Styles.createImageStyle({
        flex:1
    }),
    title: RX.Styles.createTextStyle({
      color: "black"
    }),  titleLogin: RX.Styles.createTextStyle({
      color: "black",
      fontSize:34,
      font:Fonts.displayBold,
      marginTop:20

    }), 
    titleForgot: RX.Styles.createTextStyle({
      color: "black",
      fontSize:14,
      font:Fonts.displayRegular
    }),
    InputEmail: RX.Styles.createTextInputStyle({
      color: "black",
      fontSize:14,
      font:Fonts.displayRegular,
      height:64,
      width:343,
      marginTop:20,

    }),
    InputPassword: RX.Styles.createTextInputStyle({
      color: "black",
      fontSize:14,
      font:Fonts.displayRegular,
      height:64,
      marginTop:5,

    }),
    exampleContainer: RX.Styles.createViewStyle({
      backgroundColor:'#F9F9F9'
      
    }),
    exampleContainerDark: RX.Styles.createViewStyle({
      overflow:'hidden',
    }),
    sliderContentContainer: RX.Styles.createViewStyle({
      overflow:'hidden',
    }),
    
    TextRegular: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        color: '#222222',
    }),
    TextBold: RX.Styles.createTextStyle({
        font: Fonts.displayBold,
        color: '#222222',
    }),
    TextSemi: RX.Styles.createTextStyle({
        font: Fonts.displaySemibold,
        color: '#FFFFFF',
    }),
    TextBtn: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        color: '#FFFFFF',
    }),
};

  import {  Fonts, } from '../app/Styles';

  import NavContextStore from '../stores/NavContextStore';
  import { Colors } from '../app/Styles';
  import { ComponentBase } from 'resub';
export default class ForgotPasswordPanel extends ComponentBase<CreateTodoPanelProps, CreateTodoPanelState> {
    protected _buildState(props: CreateTodoPanelProps, initState: boolean): Partial<CreateTodoPanelState>| undefined {
        let partialState: Partial<CreateTodoPanelState> = {
           width:ResponsiveWidthStore.getWidth(),
          height:ResponsiveWidthStore.getHeight(),
           };
        return partialState;
    }
    
    render() {
    return (
      <RX.View style={[_styles.exampleContainer, _styles.exampleContainerDark,{height:this.state?.height*0.9,width:this.state?.width}]}>
     <RX.Text style={[_styles.titleLogin,{width:this.state.width*0.9}]}>   Olvido contraseña</RX.Text>  
     <RX.View style={{justifyContent:'center',alignItems:'center'}}>
     <RX.Text style={{color:'black',width:this.state.width*0.9,height:50,marginTop:10}}> Porfavor introduzca una direccion de email valida. Recibiras un codigo para crea una nueva contraseña</RX.Text> 

    <RX.TextInput placeholder={'Email'} style={[_styles.InputEmail,{width:this.state.width*0.9}]}> </RX.TextInput>
    
      </RX.View>
      

        <RX.Button onPress={ this._onPressEnviar }  style={{borderRadius:50,marginTop:20,marginBottom:10,backgroundColor:Colors.menuText,alignSelf:'center',alignItems:'center',justifyContent:'center',width:this.state.width*0.9, height:48}}>
        <RX.Text style={_styles.TextBtn}> ENVIAR</RX.Text> 
       </RX.Button>

       <RX.Button onPress={ this._onPressLogin }  style={{borderRadius:50,marginTop:10,marginBottom:20,backgroundColor:Colors.menuText,alignSelf:'center',alignItems:'center',justifyContent:'center',width:this.state.width*0.9, height:48}}>
        <RX.Text style={_styles.TextBtn}> IR A INGRESO</RX.Text> 
       </RX.Button>
      </RX.View>
    );
    }
    
    private _onPressLogin = () => {
      NavContextStore.navigateToTodoList(undefined, false,false, true,false,false);
  }
 
    private _onPressEnviar = () => {
      this._enviar();
  }
  private _enviar() {
       console.log('enviado')
}
}
