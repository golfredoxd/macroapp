/*
* TopBarStack.tsx
* Copyright: Microsoft 2018
*
* Horizontal bar that appears on the top of every view within the app
* when it's using stack-based layout.
*/

import * as RX from 'reactxp';
import { ComponentBase } from 'resub';

import { Colors, Fonts, FontSizes, Styles } from '../app/Styles';
import ResponsiveWidthStore from '../stores/ResponsiveWidthStore';
import {LocaleConfig} from 'react-native-calendars';

LocaleConfig.locales['es'] = {
    monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
    monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
    dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
    dayNamesShort: ['Dim.','Lun.','Mar.','Mer.','Jeu.','Ven.','Sam.'],
  };
  LocaleConfig.defaultLocale = 'es';
const _styles = {
    background: RX.Styles.createViewStyle({
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        borderBottomWidth:1,
    }),
    leftRightContainer: RX.Styles.createViewStyle({
        flexDirection: 'row',
        alignItems: 'center',
    }),
    RightContainer: RX.Styles.createViewStyle({
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center',
        marginLeft:40,
    }),
    titleContainer: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
        flexDirection:'row',
        alignItems:'center',
    }),
    titleText: RX.Styles.createTextStyle({
        flex: -1,
        font: Fonts.displayRobotoBold,
        fontSize: 14,
        width:200,
        color: 'black',
        textAlign: 'center'
    }),
    titleText2: RX.Styles.createTextStyle({
        flex: -1,
        font: Fonts.displayRegular,
        fontSize: 12,
        width:200,
        color: 'black',
        textAlign: 'center'
    }),
    titleText4: RX.Styles.createTextStyle({
        flex: -1,
        font: Fonts.displayBold,
        fontSize: 22,
        color: 'black',
        textAlign: 'center'
    }),
    titleText3: RX.Styles.createTextStyle({
        flex: -1,
        font: Fonts.displayBold,
        fontSize: 20,
        color: 'black',
        textAlign: 'center'
    }),
    backText: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.size16,
        color: Colors.menuText,
        margin: 8
    }),
    backTextHover: RX.Styles.createTextStyle({
        color: Colors.menuTextHover
    })
};

import ImageSource from 'modules/images';
import CalendarButton from './CalendarButton';
import TodosStore from '../stores/TodosStore';
export interface TopBarStackProps extends RX.CommonProps {
    title: string;
    showBackButton: boolean;
    showLogoutButton: boolean;
    onBack?: () => void;
}

interface CreateTodoPanelState {
    width: number;
    currentDate:string;
    height:number;
}
export default class TopBarStack extends ComponentBase<TopBarStackProps, CreateTodoPanelState> {
    protected _buildState(props: TopBarStackProps, initState: boolean): Partial<CreateTodoPanelState> | undefined {
        let partialState: Partial<CreateTodoPanelState> = {
           width:ResponsiveWidthStore.getWidth(),
           height:ResponsiveWidthStore.getHeight(),
           currentDate:TodosStore.getDate(),
           };
        return partialState;
    }
    render(): JSX.Element | null {
        let leftContents: JSX.Element | undefined;
        let rightContents: JSX.Element | undefined;

        if (this.props.showBackButton) {
            leftContents = (
                <RX.Button  onPress={this._onPressBack}>
                 <RX.Image source={ImageSource.arrow} style={{height:25,width:30,marginLeft:30}}/>
            
                </RX.Button>
            );
        }
        if (this.props.showLogoutButton) {
       
        rightContents = (
         <RX.View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}> 
         
            <RX.Text style={ [_styles.titleText3,{marginLeft:40,textAlign:'center', flexDirection:'row',marginRight:15}]} numberOfLines={ 2 }>
            {this.state.currentDate }
           </RX.Text> 
           <CalendarButton /> 
</RX.View>
        );    
    }
        return (
            <RX.View style={ [_styles.background, Styles.statusBarTopMargin,{
                backgroundColor:'white',height:this.state.height*0.1}] }>
               <RX.View  style={[_styles.leftRightContainer,{
        width: this.props.title==='Todo List'?this.state.width*0.40:this.state.width*0.15,        
        }]}>
                  { leftContents }
            {this.props.title==='Todo List'?  <RX.Text style={[_styles.titleText4,{textAlign:'center', flexDirection:'row',marginLeft:70}]}>{'Mi Diario'}</RX.Text>:<RX.View/> }
   
       
           </RX.View>
                
                         {this.props.title==='Todo List'? <RX.View/> : 
                         <RX.View style={ [_styles.titleContainer, {justifyContent:'center',alignItems:'center'}]}> 
        
                   <RX.Text style={ [_styles.titleText3,{textAlign:'left',justifyContent:'center',alignSelf:'center',alignItems:'center', flexDirection:'row'}]} numberOfLines={ 2 }>
            
            { this.props.title }
                  </RX.Text>
                </RX.View>}   
                  
                <RX.View style={[_styles.RightContainer,{
        width: this.props.title==='Todo List'?this.state.width*0.50:this.state.width*0,        
        marginRight:this.state.width*0.08}]}>
                    <RX.View  style={{marginRight:this.props.title==='Todo List'?0:0}}>
                 
            {this.props.title==='Todo List'? rightContents:<RX.View/> }   
                 </RX.View>
                </RX.View>
              
            </RX.View>
        );
    }

    private _onPressBack = (e: RX.Types.SyntheticEvent) => {
        e.stopPropagation();

        if (this.props.onBack) {
            this.props.onBack();
        }
    }

}
