/**
* ViewTodoPanel.tsx
* Copyright: Microsoft 2017
*
* The Todo item edit view.
*/

import * as RX from 'reactxp';
import { ComponentBase } from 'resub';

import { FontSizes } from '../app/Styles';
import { Todo } from '../models/TodoModels';
import TodosStore from '../stores/TodosStore';

export interface ViewTodoPanelProps extends RX.CommonProps {
    todoId: string;
}

interface ViewTodoPanelState {
    todo: Todo;
    cantidad:number,
    unidad:string,
    width:number,
    group:number,
}

const _styles = {
    container: RX.Styles.createScrollViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        margin: 16,
    }),
    todoText2: RX.Styles.createTextStyle({
        margin: 8,
        width:150,
        font:Fonts.displayBold,
        fontSize: 18,
        alignSelf: 'stretch',
        backgroundColor: 'transparent',
        textAlign:'center'
    }),
    todoText: RX.Styles.createTextStyle({
        margin: 8,
        font:Fonts.displayRegular,
        fontSize: FontSizes.size16,
        alignSelf: 'stretch',
        backgroundColor: 'transparent',
        textAlign:'center'
    }),
    buttonContainer: RX.Styles.createViewStyle({
        margin: 8,
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }),
    todoImage: RX.Styles.createImageStyle({
        marginLeft: 16,
        marginRight: 16,
        height: 100,
        width: 100,
        marginBottom:20,
        marginTop:20,
    }),
    searchBox: RX.Styles.createTextInputStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.size14,
        borderWidth: 1,
        borderColor: Colors.borderSeparator,
        flex: 1,
        padding: 4,
        alignSelf:'center',
        marginLeft: 12,
    }),
    TextBtn: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        color: '#FFFFFF',
    }),
};
import { Colors, Fonts } from '../app/Styles';


import AppConfig from '../app/AppConfig';
import { GenericRestClient,ApiCallOptions } from 'simplerestclients';
import NavContextStore from '../stores/NavContextStore';
import ResponsiveWidthStore from '../stores/ResponsiveWidthStore';
export default class ViewTodoPanel extends ComponentBase<ViewTodoPanelProps, ViewTodoPanelState> {
    protected _buildState(props: ViewTodoPanelProps, initState: boolean): Partial<ViewTodoPanelState> {
        const newState: Partial<ViewTodoPanelState> = {
            todo: TodosStore.getTodoById(props.todoId),
            unidad:'gramos',
            group:TodosStore.getGroup(),
            width:ResponsiveWidthStore.getWidth()
        };

        return newState;
    }

    render() {
        return ( 
            <RX.ScrollView  style={ _styles.container }>
                <RX.View style={{flex:1,justifyContent:'center',
        alignItems:'center'}}>
            <RX.View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',flex:1}}>
                 <RX.Image
            style={ _styles.todoImage }
            source={ this.state.todo.food_pic }
        />
        
                    <RX.View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                
                    <RX.View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

<RX.Text style={ _styles.todoText }>
    {' Nombre: '}
</RX.Text>
                <RX.Text numberOfLines={3} style={ _styles.todoText2 }>
                    { this.state.todo ? this.state.todo.nombre : '' }
                </RX.Text>
</RX.View>

<RX.View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

<RX.Text style={ _styles.todoText }>
    {' Descripcion: '}
</RX.Text>
                <RX.Text  numberOfLines={3} style={ _styles.todoText2 }>
                    { this.state.todo ? this.state.todo.descripcion : '' }
                </RX.Text>
</RX.View>
                
                </RX.View>
                
                </RX.View>
                <RX.View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                <RX.Text style={ _styles.todoText }>
                    {' Cantidad de Fibra: '}
                </RX.Text>
                <RX.Text style={ _styles.todoText }>
                    { this.state.todo ? this.state.todo.fibra.toFixed(2)+' gr' : '' }
                </RX.Text>
                </RX.View>
                <RX.View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                <RX.Text style={ _styles.todoText }>
                    {' Cantidad de Proteinas: '}
                </RX.Text>
                <RX.Text style={ _styles.todoText }>
                    { this.state.todo ? this.state.todo?.proteinas.toFixed(2)+' gr' : 0 +' gr' }
                </RX.Text>
                </RX.View>
                
                <RX.View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                <RX.Text style={ _styles.todoText }>
                    {' Cantidad de Carbohidratos: '}
                </RX.Text>
                <RX.Text style={ _styles.todoText }>
                    { this.state.todo ? this.state.todo?.carbohidratos?.toFixed(2)+' gr' : 0 +' gr' }
                </RX.Text>
                </RX.View>
                
                <RX.View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                <RX.Text style={ _styles.todoText }>
                    {' Cantidad de Grasas: '}
                </RX.Text>
                <RX.Text style={ _styles.todoText }>
                    { this.state.todo ? this.state.todo?.grasas.toFixed(2)+' gr' : 0 +' gr' }
                </RX.Text>
                </RX.View>
                
                <RX.View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                <RX.Text style={ _styles.todoText }>
                    {' Cantidad de Calorias: '}
                </RX.Text>
                <RX.Text style={ _styles.todoText }>
                    { this.state.todo ? this.state.todo?.calorias.toFixed(2)+' gr' : 0 +' gr' }
                </RX.Text>
                </RX.View>
                <RX.View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                <RX.Text style={ _styles.todoText }>
                    {' Cantidad de Sodio: '}
                </RX.Text>
                <RX.Text style={ _styles.todoText }>
                    { this.state.todo ? this.state.todo?.sodio.toFixed(2)+' gr' : 0 +' gr' }
                </RX.Text>
                </RX.View>
                </RX.View>
                
        <RX.TextInput
                        style={ [_styles.searchBox,{width:this.state.width*0.7,marginTop:20,marginBottom:20}] }
                        value={ this.state.cantidad?.toString() }
                        autoFocus={ !AppConfig.isTouchInterface() }
                        placeholder={ 'Indique la Cantidad de Alimento' }
                        onChangeText={ this._onChangeTextSearch }
                        autoCapitalize={ 'none' }
                    />
                <RX.View style={ _styles.buttonContainer }>
                    
            <RX.Button onPress={ this._onPressDelete }  style={{borderRadius:40,marginBottom:20,backgroundColor:'#4e58ca',alignSelf:'center',alignItems:'center',justifyContent:'center',width:this.state.width*0.8, height:50}}>
    <RX.Text style={_styles.TextBtn}> {'AGREGAR ALIMENTO'}</RX.Text> 
            </RX.Button>
                    </RX.View>
            </RX.ScrollView>
        );
    }

    private _onChangeTextSearch = (newValue: string) => {
        this.setState({
            cantidad: parseFloat(newValue),
        })
                  
    };
    private _onPressDelete = (e: RX.Types.SyntheticEvent) => {
            
           

            const headers = {
            'Authorization': `Bearer ${TodosStore.getToken()}`,
            };

            class Http extends GenericRestClient {
            protected _getHeaders(options: ApiCallOptions): { [header: string]: string } {
                return headers;
            }
            }

            const http = new Http('http://ezeros.com:3001/api');
                let fecha=TodosStore.getDate2()
                let user=TodosStore.getUserId()
                let grams= this.state?.cantidad
                http.performApiPost(`/diary/save`, {user_id:user,food_id:this.state.todo.id,group_id:this.state.group,fecha:fecha,cantidad:grams})
                .then((resp:any)=>{                  
    
         
                    http.performApiGet(`/diary/byDate/${TodosStore.getUserId()}/${TodosStore.getDate2()}`,{ contentType: 'json' }).then((respuesta2:any )=> {
 
                        TodosStore.setTodos2(respuesta2.body)
                                                    
                    http.performApiGet(`/diary/TotalbyDate/${TodosStore.getUserId()}/${TodosStore.getDateToday()}`,{ contentType: 'json' }).then((respuesta5:any )=> {
                        let goal= {
                        total:respuesta5.body.total,
                        fibra:respuesta5.body.fibra,
                        
                        
                        grasas:respuesta5.body.grasas,
                        
                        proteinas:respuesta5.body.proteinas,
                        
                        carbohidratos:respuesta5.body.carbohidratos,
                        
                        }
                        TodosStore.setGoal2(goal)
                                        });              
                        http.performApiGet(`/user/goal/${TodosStore.getUserId()}`,{ contentType: 'json' }).then((respuesta:any )=> {
                            let goal= {
                            fibra:parseFloat(respuesta.body.fibra),
                            
                            calorias:parseFloat(respuesta.body.calorias),
                            
                            grasas:parseFloat(respuesta.body.grasas),
                            
                            proteinas:parseFloat(respuesta.body.proteinas),
                            
                            carbohidratos:parseFloat(respuesta.body.carbohidratos),
                            
                            }
                            
                            TodosStore.setGoal(goal);
                            
                            NavContextStore.navigateToTodoList();
                                    });
                                 });
                                 
    
            });
            
}
}