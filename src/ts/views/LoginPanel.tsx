/**
* CreateTodoPanel.tsx
* Copyright: Microsoft 2017
*
* The Todo item edit view.
*/

import * as RX from 'reactxp';
import ResponsiveWidthStore from '../stores/ResponsiveWidthStore';

import {  Fonts, } from '../app/Styles';


import { GenericRestClient,ApiCallOptions } from 'simplerestclients';
import NavContextStore from '../stores/NavContextStore';
import { ComponentBase } from 'resub';

import TodosStore  from '../stores/TodosStore';
import CurrentUserStore from '../stores/CurrentUserStore';



import { User } from '../models/IdentityModels';

interface CreateTodoPanelProps extends RX.CommonProps {

}

interface CreateTodoPanelState {
    width: number;
    height:number;
    password:string;
    errorLogin:boolean;
    email:string;
}

const _styles = {
    container: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        padding: 16
    }),
    slider: RX.Styles.createViewStyle({
      overflow:'hidden'
    }),
    slide: RX.Styles.createViewStyle({

      }),
    todoImage: RX.Styles.createImageStyle({
        flex:1
    }),
    TextBtn: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        color: '#FFFFFF',
    }),
    TextBtn2: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        color: '#0C152E',
        fontSize:16
    }),
    title: RX.Styles.createTextStyle({
      color: "black"
    }),  titleLogin: RX.Styles.createTextStyle({
      color: "black",
      fontSize:24,
      font:Fonts.displayBold,
      marginTop:20,
    textAlign:'center'
    }), 
    titleForgot: RX.Styles.createTextStyle({
      color: "black",
      fontSize:14,
      marginTop:5,
      font:Fonts.displayRegular,
      textAlign:'left'
    }),
    InputEmail: RX.Styles.createTextInputStyle({
      color: "black",
      fontSize:14,
      font:Fonts.displayRegular,
      height:50,
      width:343,
      paddingLeft:20,
      marginTop:30,
      borderRadius:40,
      borderWidth:1

    }),
    InputPassword: RX.Styles.createTextInputStyle({
      color: "black",
      fontSize:14,
      font:Fonts.displayRegular,
      height:50,
      marginTop:10,
      width:343,
      paddingLeft:20,
      borderRadius:40,
      borderWidth:1

    }),
    exampleContainer: RX.Styles.createViewStyle({
      backgroundColor:'#F9F9F9'
      
    }),
    sliderContentContainer: RX.Styles.createViewStyle({
      overflow:'hidden',
    }),
    
    TextRegular: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        color: '#222222',
    }),
    TextBold: RX.Styles.createTextStyle({
        font: Fonts.displayBold,
        color: '#222222',
    }),
    TextSemi: RX.Styles.createTextStyle({
        font: Fonts.displaySemibold,
        color: '#FFFFFF',
    }),
    titleText4: RX.Styles.createTextStyle({
        flex: -1,
        font: Fonts.displayBold,
        fontSize: 26,
        color: 'black',
        textAlign: 'center'
    }),
};


import ImageSource from 'modules/images';

export default class LoginPanel extends ComponentBase<CreateTodoPanelProps, CreateTodoPanelState> {
   
  protected _buildState(props: CreateTodoPanelProps, initState: boolean): Partial<CreateTodoPanelState>| undefined {
        let partialState: Partial<CreateTodoPanelState> = {
           width:ResponsiveWidthStore.getWidth(),
          height:ResponsiveWidthStore.getHeight(),

           };
        return partialState;
    }
    
    render() {
    return (
      <RX.View style={[_styles.exampleContainer,{flex:1,justifyContent:'center',alignItems:'center'}]}>
            
          <RX.View style={{flex:1,alignSelf:'center',justifyContent:'center',alignItems:'center'}}>
        <RX.View style={{justifyContent:'center',alignItems:'center',height:100,marginBottom:60,flexDirection:'row'}}>
        <RX.Image style={{height:75,width:75,marginRight:10}} source={ImageSource.logo}/>
          <RX.Text style={ [_styles.titleText4,{textAlign:'center', flexDirection:'row',marginRight:10}]} numberOfLines={ 2 }>
            
            { 'Julio Valero' }
                  </RX.Text>
                  </RX.View>
          <RX.Text style={_styles.TextBtn2}>{'Ingresa Tus Datos'}</RX.Text> 
          <RX.TextInput value={this.state.email}  placeholder={ 'Enter Email' }  onChangeText={ val => this.setState({ email: val }) } style={[_styles.InputEmail,{width:this.state.width*0.75}]} /> 
        
          <RX.TextInput secureTextEntry={true}  value={this.state.password}  placeholder={ 'Enter Password' } onChangeText={ val => this.setState({ password: val })}  style={[_styles.InputPassword,{width:this.state.width*0.75}]}  />
          <RX.View style={{width:this.state.width,justifyContent:'center',alignItems:'flex-end'}}>
            {this.state.errorLogin===true?<RX.Text style={[_styles.TextBtn2,{alignSelf:'center',color:'red'}]}>{'Informacion Invalida'}</RX.Text> :null}
            <RX.Button style={{justifyContent:'center',alignItems:'flex-end',marginRight:70,width:140,height:50}} onPress={ this._onPressForgotPassword }  >
            <RX.Text style={[_styles.titleForgot,{textAlign:'right',marginTop:25,marginBottom:45,}]}>  {'¿Olvido Contraseña?'}</RX.Text>  
            </RX.Button>
            
            <RX.Button onPress={ this._onPressLogin }  style={{borderRadius:40,marginBottom:20,backgroundColor:'#1273EB',alignSelf:'center',alignItems:'center',justifyContent:'center',width:this.state.width*0.8, height:50}}>
    <RX.Text style={_styles.TextBtn}> {'INICIAR SESION'}</RX.Text> 
            </RX.Button>
            </RX.View>
            </RX.View>
           
      </RX.View>
    );
    }
    
 

  private _onPressForgotPassword = () => {
    this._forgot();
}
private _forgot() {
      NavContextStore.navigateToTodoList(undefined, false,false, false,false,true);
}
    private _onPressLogin = () => {
      
      new GenericRestClient('http://ezeros.com:3001/api').performApiPostDetailed('/auth/login', {username:this.state.email, password:this.state.password}).promise.then(resp => this.loginIn(resp)).catch((res)=>{
 
 if(res.body.body==="Informacion invalida"){
  this.setState({errorLogin:true})};
 }
      )

  }
  
  private loginIn(resp:any) {
RX.Storage.setItem('token',resp.body.body.jwt)
TodosStore.setDate()
RX.Storage.setItem('user_id',resp.body.body.user_id)
TodosStore.setToken(resp.body.body.jwt)
const headers = {
  'Authorization': `Bearer ${TodosStore.getToken()}`,
};

class Http extends GenericRestClient {
  protected _getHeaders(options: ApiCallOptions): { [header: string]: string } {
      return headers;
  }
}


const http = new Http('http://ezeros.com:3001/api');
TodosStore.setUserId(resp.body.body.user_id)
   http.performApiGet(`/diary/TotalbyDate/${resp.body.body.user_id}/${TodosStore.getDateToday()}`,{ contentType: 'json' }).then((respuesta5:any )=> {
    let goal= {
      total:respuesta5.body.total,
      fibra:respuesta5.body.fibra,
      
      
      grasas:respuesta5.body.grasas,
      
      proteinas:respuesta5.body.proteinas,
      
      carbohidratos:respuesta5.body.carbohidratos,
      
    }
    TodosStore.setGoal2(goal)
                    });
                    
http.performApiGet(`/diary/byDate/${resp.body.body.user_id}/${TodosStore.getDateToday()}`,{ contentType: 'json' }).then((respuesta2:any )=> {
 
  TodosStore.setTodos2(respuesta2.body)
           });
http.performApiGet(`/user/group/${resp.body.body.user_id}`,{ contentType: 'json' }).then((respuesta1:any )=> {
 TodosStore.setGroups(respuesta1.body)
          });

          http.performApiGet(`/user/profile/${resp.body.body.user_id}`,{ contentType: 'json' }).then((respuesta8:any )=> {
            let user:User={
              id: respuesta8.body.id,
              firstname:  respuesta8.body.firstname,
              lastname:  respuesta8.body.lastname,
              username: respuesta8.body.username,
              email: respuesta8.body.email,
              profile_pic: respuesta8.body.profile_pic,
              modo: respuesta8.body.modo,
              groups:{
                  1:respuesta8.body.groups[1],
                  2: respuesta8.body.groups[2],
                  3: respuesta8.body.groups[3]
              },
              goals:{
                  fibra: respuesta8.body.goals.fibra,
                  calorias: respuesta8.body.goals.calorias,
                  grasas: respuesta8.body.goals.grasas,
                  proteinas: respuesta8.body.goals.proteinas,
                  carbohidratos: respuesta8.body.goals.carbohidratos,
                  role:respuesta8.body.goals.role
              }
            }
            CurrentUserStore.setUser(user)
                     });
http.performApiGet(`/user/goal/${resp.body.body.user_id}`,{ contentType: 'json' }).then((respuesta:any )=> {
let goal= {
  fibra:parseFloat(respuesta.body.fibra),
  
  calorias:parseFloat(respuesta.body.calorias),
  
  grasas:parseFloat(respuesta.body.grasas),
  
  proteinas:parseFloat(respuesta.body.proteinas),
  
  carbohidratos:parseFloat(respuesta.body.carbohidratos),
  
}

TodosStore.setGoal(goal);
        });
    NavContextStore.navigateToTodoList();

}

}