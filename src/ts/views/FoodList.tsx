
/**
* TodoListPanel.tsx
* Copyright: Microsoft 2018
*
* Display first screen of the Todo application.
*/

import * as _ from 'lodash';
import * as RX from 'reactxp';
import { VirtualListView, VirtualListViewItemInfo } from 'reactxp-virtuallistview';
import { VirtualListCellRenderDetails } from 'reactxp-virtuallistview/dist/VirtualListCell';
import { ComponentBase } from 'resub';

import AppConfig from '../app/AppConfig';
import { Colors, Fonts, FontSizes } from '../app/Styles';

import { Todo } from '../models/TodoModels';
import TodosStore from '../stores/TodosStore';

import TodoListItem from './TodoListItem';

interface TodoListItemInfo extends VirtualListViewItemInfo {
    todo: Todo;
}

export interface TodoListPanelProps extends RX.CommonProps {
    selectedTodoId?: string;
    onSelect: (selectedId: string) => void;
    onCreateNew: () => void;
}

interface TodoListPanelState {
    todos: TodoListItemInfo[];
    filteredTodoList: TodoListItemInfo[];
    searchString: string;
    
}

const _listItemHeight = 48;

const _styles = {
    listScroll: RX.Styles.createViewStyle({
        flexDirection: 'column',
        alignSelf: 'stretch',
        backgroundColor: Colors.contentBackground,
    }),
    todoListHeader: RX.Styles.createViewStyle({
        height: 60,
        borderBottomWidth: 1,
        borderColor: Colors.borderSeparator,
        flexDirection: 'row',
        alignItems: 'center',
    }),
    searchBox: RX.Styles.createTextInputStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.size14,
        borderWidth: 1,
        borderColor: Colors.borderSeparator,
        flex: 1,
        padding: 4,
        marginLeft: 12,
        marginRight: 12,
    }),
    container: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: Colors.contentBackground,
    }),
    addTodoButton: RX.Styles.createViewStyle({
        margin: 8,
        paddingHorizontal: 8,
        paddingVertical: 4,
    }),
    buttonText2: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        fontSize: 20,
        lineHeight: 32,
        color: 'black',
    }),
    buttonText: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.size32,
        lineHeight: 32,
        color: Colors.buttonTextColor,
    }),
    buttonTextHover: RX.Styles.createTextStyle({
        color: Colors.buttonTextHover,
    }),
};

import { GenericRestClient,ApiCallOptions } from 'simplerestclients';


import ImageSource from 'modules/images';

export default class FoodList extends ComponentBase<TodoListPanelProps, TodoListPanelState> {
    protected _buildState(props: TodoListPanelProps, initState: boolean): Partial<TodoListPanelState> | undefined {
        const partialState: Partial<TodoListPanelState> = {
        };
        partialState.todos = TodosStore.getTodos().map((todo, i) => ({
            key: i.toString(),
            height: _listItemHeight,
            template: 'todo',
            todo,
        }));

        if (initState) {
            partialState.searchString = '';
            partialState.filteredTodoList = partialState.todos;
        } else {
            const filter = _.trim(partialState.searchString);
            if (filter) {
                partialState.filteredTodoList = this._filterTodoList(partialState.todos, filter);
            } else {
                partialState.filteredTodoList = partialState.todos;
            }
        }

        return partialState;
    }
    render() {
        return (
            <RX.View useSafeInsets={ true } style={ _styles.container }>
                <RX.View style={ _styles.todoListHeader }>
                    <RX.TextInput
                        style={ _styles.searchBox }
                        value={ this.state.searchString }
                        autoFocus={ !AppConfig.isTouchInterface() }
                        placeholder={ 'Search' }
                        onChangeText={ this._onChangeTextSearch }
                        autoCapitalize={ 'none' }
                    />
                </RX.View>

{this.state.filteredTodoList.length===0?
<RX.View style={{flex:1,marginBottom:30,justifyContent:'center',alignItems:'center'}}>
<RX.Image source={ImageSource.wait} style={{marginBottom:10,height:100,width:100}}/>
<RX.Text style={_styles.buttonText2}>
    {'Busca el alimento que desees...'}
</RX.Text></RX.View>:
  <VirtualListView
  itemList={ this.state.filteredTodoList }
  renderItem={ this._renderItem }
  style={ _styles.listScroll }
/>
}
              
            </RX.View>
        );
    }

    private _onChangeTextSearch = (newValue: string) => {

        const headers = {
          'Authorization': `Bearer ${TodosStore.getToken()}`,
        };
        
        class Http extends GenericRestClient {
          protected _getHeaders(options: ApiCallOptions): { [header: string]: string } {
              return headers;
          }
        }
        

        const http = new Http('http://ezeros.com:3001/api');
        
        
        
        http.performApiGet(`/search/food/${newValue}`,{ contentType: 'json' }).then((resp:any )=> {
        
                this.setState({
                searchString: newValue,
            })
            TodosStore.setTodos(resp.body)});
            
    };
    private _filterTodoList(sortedTodos: TodoListItemInfo[], searchString: string): TodoListItemInfo[] {
        const 
        lowerSearchString = searchString.toLowerCase();

        return _.filter(sortedTodos, item => {
            const todoLower = item.todo.nombre?.toLowerCase();
            return todoLower.search(lowerSearchString) >= 0;
        });
    }

    private _renderItem = (details: VirtualListCellRenderDetails<TodoListItemInfo>) => {
        const item = details.item;
        return (
            <TodoListItem
                todo={ item.todo }
                height={ _listItemHeight }
                isSelected={ item.todo.id === this.props.selectedTodoId }
                searchString={ this.state.searchString }
                onPress={ this._onPressTodo }
            />
        );
    };

    private _onPressTodo = (todoId: string) => {
        
        this.props.onSelect(todoId);
        this.setState({
            searchString: '',
            filteredTodoList: this.state.todos,
        });
    };

}
