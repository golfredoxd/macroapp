/*
* AccountMenuButton.tsx
* Copyright: Microsoft 2018
*
* Button that displays the currently-signed-in user and provides
* a popup menu that allows the user to sign out, adjust account
* settings, etc.
*/

import * as RX from 'reactxp';
import { ComponentBase } from 'resub';

import { GenericRestClient,ApiCallOptions } from 'simplerestclients';
import CurrentUserStore from '../stores/CurrentUserStore';
import { Colors, Fonts, FontSizes } from '../app/Styles';

interface AccountMenuButtonState {
    currentUserName: string;
    isHovering: boolean;
    currentDate: string;
}

const _menuPopupId = 'accountMenu';

const _styles = {
    buttonContainer: RX.Styles.createButtonStyle({
        flexDirection: 'row',
        alignItems: 'center',
    }),
    nameText: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.menuItem,
        marginHorizontal: 8,
        color: Colors.menuText,
    }),
    nameTextHover: RX.Styles.createTextStyle({
        color: Colors.menuTextHover,
    }),
    circleGlyph: RX.Styles.createViewStyle({
        width: 12,
        height: 12,
        borderRadius: 6,
        borderWidth: 1,
        borderColor: Colors.menuText,
        backgroundColor: Colors.logoColor,
    }),
    circleGlyphHover: RX.Styles.createViewStyle({
        borderColor: Colors.menuTextHover,
    }),
};
import {LocaleConfig, DateObject} from 'react-native-calendars';

import {Calendar} from 'react-native-calendars';
import ImageSource from 'modules/images';
import TodosStore from '../stores/TodosStore';
LocaleConfig.locales.es = {
    monthNames: [
      'Enero',
      'Febrero',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
      'Julio',
      'Agosto',
      'Septiembre',
      'Octubre',
      'Noviembre',
      'Diciembre',
    ],
    monthNamesShort: [
      'Ene.',
      'Feb.',
      'Mar',
      'Abr',
      'May',
      'Jun',
      'Jul.',
      'Ago',
      'Sep.',
      'Oct.',
      'Nov.',
      'Dic.',
    ],
    dayNames: [
      'Domingo',
      'Lunes',
      'Martes',
      'Miercoles',
      'Jueves',
      'Viernes',
      'Sabado',
    ],
    dayNamesShort: ['Dom.', 'Lun.', 'Mar.', 'Mie.', 'Jue.', 'Vie.', 'Sab.'],
  };
  
LocaleConfig.defaultLocale = 'es';

export default class CalendarButton extends ComponentBase<RX.CommonProps, AccountMenuButtonState> {
    private _mountedButton: any;

    protected _buildState(props: RX.CommonProps, initState: boolean): Partial<AccountMenuButtonState> | undefined {
        const partialState: Partial<AccountMenuButtonState> = {
            currentUserName: CurrentUserStore.getFullName(),
            currentDate:TodosStore.getDate(),
        };

        return partialState;
    }

    render(): JSX.Element | null {
        return (
            <RX.Button
                ref={ this._onMountButton }
                style={ _styles.buttonContainer }
                onPress={ this._onPress }
                onHoverStart={ () => this.setState({ isHovering: true }) }
                onHoverEnd={ () => this.setState({ isHovering: false }) }
            >
                 <RX.Image source={ImageSource.calendar} style={{height:75,width:25}}>   
      
      </RX.Image>
            </RX.Button>
        );
    }
    private _onDayPress(date:DateObject){
       TodosStore.setTodoDate(date)

       this.setState({
           currentDate:date.year + '/' + date.month  + '/' +  date.day
       })
       

       const headers = {
        'Authorization': `Bearer ${TodosStore.getToken()}`,
        };

        class Http extends GenericRestClient {
        protected _getHeaders(options: ApiCallOptions): { [header: string]: string } {
            return headers;
        }
        }
        
        const http = new Http('http://ezeros.com:3001/api');

http.performApiGet(`/diary/byDate/${TodosStore.getUserId()}/${date.year+'-'+date.month+'-'+date.day}`,{ contentType: 'json' }).then((respuesta2:any )=> {
 
    TodosStore.setTodos2(respuesta2.body)
             });
       RX.Popup.dismiss(_menuPopupId)
    }
    private _onMountButton = (elem: any) => {
        this._mountedButton = elem;
    };
    private _onPress = (e: RX.Types.SyntheticEvent) => {
        e.stopPropagation();

        RX.Popup.show({
            getAnchor: () => this._mountedButton,
            getElementTriggeringPopup: () => this._mountedButton,
            renderPopup: (anchorPosition: RX.Types.PopupPosition, anchorOffset: number, popupWidth: number, popupHeight: number) => {
                

                return (
                    <RX.View style={{width:RX.UserInterface.measureWindow().width}}>
                <Calendar
  // Specify style for calendar container element. Default = {}
  style={{
    borderWidth: 1,
    borderColor: 'gray',
    height: 350
  }}
  //current date
  current={this.state.currentDate}
 
onDayPress={(date:DateObject)=>this._onDayPress(date)}
  // Specify theme properties to override specific styles for calendar parts. Default = {}
  theme={{
    backgroundColor: '#ffffff',
    calendarBackground: '#ffffff',
    textSectionTitleColor: '#b6c1cd',
    selectedDayBackgroundColor: '#00adf5',
    selectedDayTextColor: '#ffffff',
    todayTextColor: '#00adf5',
    dayTextColor: '#2d4150',
    textDisabledColor: '#d9e1e8',
    dotColor: '#00adf5',
    selectedDotColor: '#ffffff',
    arrowColor: 'orange',
    monthTextColor: 'blue',
    indicatorColor: 'blue',
    textDayFontFamily: 'monospace',
    textMonthFontFamily: 'monospace',
    textDayHeaderFontFamily: 'monospace',
    textDayFontWeight: '300',
    textMonthFontWeight: 'bold',
    textDayHeaderFontWeight: '300',
    textDayFontSize: 16,
    textMonthFontSize: 16,
    textDayHeaderFontSize: 16
  }}
/>
                </RX.View>
                );
            },
            dismissIfShown: true,
        }, _menuPopupId);
    };

}
