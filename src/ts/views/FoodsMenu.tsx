/*
* AccountMenuButton.tsx
* Copyright: Microsoft 2018
*
* Button that displays the currently-signed-in user and provides
* a popup menu that allows the user to sign out, adjust account
* settings, etc.
*/

import * as RX from 'reactxp';
import { ComponentBase } from 'resub';

import SimpleMenu from '../controls/SimpleMenu2';
import { Colors, Fonts, FontSizes } from '../app/Styles';

import { Todo2 } from '../models/TodoModels';
interface AccountMenuButtonState {
    currentUserName: string;
    isHovering: boolean;
    width:number;
    nuevoNombre:string;
}

interface AccountMenuButtonProps{
    todos: Todo2[];
    title:string;
    onPress:()=>void;
}
const _menuPopupId = 'accountMenu';
const _styles = {
    buttonContainer: RX.Styles.createButtonStyle({
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth:0.2,
    }),
    nameText: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.menuItem,
        marginHorizontal: 8,
        color: Colors.menuText,
    }),
    nameTextHover: RX.Styles.createTextStyle({
        color: Colors.menuTextHover,
    }),
    circleGlyph: RX.Styles.createViewStyle({
        width: 12,
        height: 12,
        borderRadius: 6,
        borderWidth: 1,
        borderColor: Colors.menuText,
        backgroundColor: Colors.logoColor,
    }),
    circleGlyphHover: RX.Styles.createViewStyle({
        borderColor: Colors.menuTextHover,
    }),
    title:RX.Styles.createTextStyle({
        marginLeft:60,fontSize:16,font:Fonts.displayLight,

    }),
    searchBox: RX.Styles.createTextInputStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.size14,
        borderWidth: 1,
        borderColor: Colors.borderSeparator,
        flex: 1,
        padding: 4,
        marginTop:30,
        marginLeft: 12,
    }),
};

import AppConfig from '../app/AppConfig';

import SimpleDialog from '../controls/SimpleDialog';

import ImageSource from 'modules/images';

import NavContextStore from '../stores/NavContextStore';
import ResponsiveWidthStore from '../stores/ResponsiveWidthStore';

import { GenericRestClient,ApiCallOptions } from 'simplerestclients';
import TodosStore from '../stores/TodosStore';

const _confirmDeleteDialogId = 'nombre';
export default class FoodsMenu extends ComponentBase<AccountMenuButtonProps, AccountMenuButtonState> {
    private _mountedButton: any;

    protected _buildState(props: AccountMenuButtonProps, initState: boolean): Partial<AccountMenuButtonState> | undefined {
        const partialState: Partial<AccountMenuButtonState> = {
         
            width:ResponsiveWidthStore.getWidth(),
        };
        return partialState;
    }

    render(): JSX.Element | null {
        return (
            <RX.Button
                ref={ this._onMountButton }
                style={ _styles.buttonContainer }
                onHoverStart={ () => this.setState({ isHovering: true }) }
                onHoverEnd={ () => this.setState({ isHovering: false }) }
            >
                <RX.View style={{flexDirection:'row',height:60,backgroundColor:'white',width:this.state.width,flex:1,justifyContent:'flex-start',alignItems:'center',alignSelf:'center'}}>
              <RX.View style={{flex:40}}>
                <RX.Text style={_styles.title}>
                    {this.props.title}
                    </RX.Text>
                    </RX.View>
                    <RX.View style={{flex:60,flexDirection:'row',alignSelf:'center',justifyContent:'center'}}>
                      <RX.Button  onPress={this._onPress2}>
                      <RX.Image
                    style={ {height:16,width:16,marginLeft:30,marginRight:25} }
                    source={ImageSource.plus2 }
                />
                          </RX.Button>
                          <RX.Button  onPress={this._onPressDelete}>
                          <RX.Image
                    style={ {height:16,width:16,marginRight:25} }
                    source={ImageSource.dots }
                />
                          </RX.Button>
                          <RX.Button onPress={this._onPress}>
                     <RX.Image
                    style={ {height:16,width:16} }
                    source={ImageSource.down }
                />
                          </RX.Button>
                         </RX.View>
                         </RX.View>
            </RX.Button>
        );
    }


    private _onChangeTextSearch = (newValue: string) => {
        this.setState({
            nuevoNombre: newValue,
        })
                  
    };
    private _onPressDelete = (e: RX.Types.SyntheticEvent) => {
        e.stopPropagation();

        const dialog = (
            <SimpleDialog
                dialogId={ _confirmDeleteDialogId }
                text={ 'Nuevo Nombre' }
                children={
                <RX.TextInput
                style={ _styles.searchBox }
                value={ this.state.nuevoNombre?.toString() }
                autoFocus={ !AppConfig.isTouchInterface() }
                placeholder={ 'Dale un nuevo nombre al grupo' }
                onChangeText={ this._onChangeTextSearch }
                autoCapitalize={ 'none' }
            />
                }
                buttons={ [{
                    text: 'Aceptar',
                    onPress: () => {
                        SimpleDialog.dismissAnimated(_confirmDeleteDialogId);
                        this._completeDelete();
                    },
                },{
                    text: 'Cancelar',
                    isCancel: true,
                    onPress: () => {
                        SimpleDialog.dismissAnimated(_confirmDeleteDialogId);
                    }}] }
            />
        );

        RX.Modal.show(dialog, _confirmDeleteDialogId);
    };
    private _completeDelete() {
        this.props.onPress()
        const headers = {
            'Authorization': `Bearer ${TodosStore.getToken()}`,
            };

            class Http extends GenericRestClient {
            protected _getHeaders(options: ApiCallOptions): { [header: string]: string } {
                return headers;
            }
            }

            const http = new Http('http://ezeros.com:3001/api');
            if(TodosStore.getGroup()===1){
                
                http.performApiPost(`/user/group`, {1:this.state.nuevoNombre,2:TodosStore.getFoodGroup()[2],3:TodosStore.getFoodGroup()[3]})
                .then((resp:any)=>{              
http.performApiGet(`/user/group/${TodosStore.getUserId()}`,{ contentType: 'json' }).then((respuesta1:any )=> {
    TodosStore.setGroups(respuesta1.body)
    
    NavContextStore.navigateToTodoList();
             });
    
            });
            }else if(TodosStore.getGroup()===2){
                http.performApiPost(`/user/group`, {1:TodosStore.getFoodGroup()[1],2:this.state.nuevoNombre,3:TodosStore.getFoodGroup()[3]})
                .then((resp:any)=>{                  
              
                    console.log(JSON.stringify(resp));
                    NavContextStore.navigateToTodoList();
    
            });
            }else if(TodosStore.getGroup()===3){
                http.performApiPost(`/user/group`, {1:TodosStore.getFoodGroup()[1],2:TodosStore.getFoodGroup()[2],3:this.state.nuevoNombre})
                .then((resp:any)=>{                  
              
                    console.log(JSON.stringify(resp));
                    NavContextStore.navigateToTodoList();
    
            });
            }
              
    }
    private _onMountButton = (elem: any) => {
        this._mountedButton = elem;
    };


    private _onPress2 = () => {
      
        this.props.onPress()
        NavContextStore.navigateToTodoList(undefined,false,false,false,false,false,false,false,true)
     
    };

    private _onPress = (e: RX.Types.SyntheticEvent) => {
        e.stopPropagation();

        RX.Popup.show({
            getAnchor: () => this._mountedButton,
            getElementTriggeringPopup: () => this._mountedButton,
            renderPopup: (anchorPosition: RX.Types.PopupPosition, anchorOffset: number, popupWidth: number, popupHeight: number) => {
               
                return (
                    <SimpleMenu
                        todos={ this.props.todos }
                        onSelectItem={ this._onSelectMenuItem }
                    />
                );
            },
            dismissIfShown: true,
        }, _menuPopupId);
    };

    private _onSelectMenuItem = (command: string) => {
        RX.Popup.dismiss(_menuPopupId);
        if(command==='profile'){

            NavContextStore.navigateToTodoList(undefined,false,false,false,false,false,false,false,false,true);
        } else if(command==='signout'){

                NavContextStore.navigateToTodoList(undefined, false,false, true,false);
        
        }
        // TODO - need to implement
    };
}
