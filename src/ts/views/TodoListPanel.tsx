/**
* TodoListPanel.tsx
* Copyright: Microsoft 2018
*
* Display first screen of the Todo application.
*/

import * as _ from 'lodash';
import * as RX from 'reactxp';
import * as React from 'react';import { ComponentBase } from 'resub';
import { Colors, Fonts, FontSizes } from '../app/Styles';

import ResponsiveWidthStore from '../stores/ResponsiveWidthStore';

import { Goal2,Goal1,Todo2,Group } from '../models/TodoModels';

export interface TodoListPanelProps extends RX.CommonProps {
    selectedTodoId?: string;
    onSelect: (selectedId: string) => void;
    onCreateNewWork: () => void;
    onCreateNewNews: () => void;
    onCreateNewRecent: () => void;
}

interface TodoListPanelState {
  
  todosDe: Todo2[];
  todosAl: Todo2[];
  todosCe: Todo2[];
      todos: Todo2[];
    width:number;
    goal1:Goal1;
    goal2:Goal2;
    activeSlide:number;
    height:number;
    filteredTodoList: [];
    searchString: string;
    entries:Entries[];
    groupFoods:Group;
    group:number;
    isConsumido:boolean;
}

import ImageSource from 'modules/images';

const _styles = {  
    slide: RX.Styles.createViewStyle({
    backgroundColor: "white",
    height: 100,
  }),
  title: RX.Styles.createTextStyle({
    color: "black"
  }),
  exampleContainer: RX.Styles.createViewStyle({
    height: 300,
  }),
  exampleContainerDark: RX.Styles.createViewStyle({
    backgroundColor: "#ddd"
  }),
  slider: RX.Styles.createViewStyle({
    overflow: 'visible' // for custom animations
  }),
  sliderContentContainer: RX.Styles.createViewStyle({
    paddingVertical: 5, // for custom animation
   
  }),
    listScroll: RX.Styles.createViewStyle({
        flexDirection: 'column',
        alignSelf: 'stretch',
        height:200,
        backgroundColor: Colors.contentBackground,
    }),
    todoListHeader: RX.Styles.createViewStyle({
        height: 60,
        borderBottomWidth: 1,
        borderColor: Colors.borderSeparator,
        flexDirection: 'row',
        alignItems: 'center',
    }),
    TextBtn2: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        color: 'black',
    }),
    TextBtn: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        color: '#FFFFFF',
    }),
    searchBox: RX.Styles.createTextInputStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.size14,
        borderWidth: 1,
        borderColor: Colors.borderSeparator,
        flex: 1,
        padding: 4,
        marginLeft: 12,
        marginRight: 12,
    }),
    container: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: 'white',
    }),
    addTodoButton: RX.Styles.createViewStyle({
        margin: 8,
        paddingHorizontal: 8,
        alignSelf:'flex-end'
    }),
    buttonText: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.size32,
        lineHeight: 32,
        color: Colors.buttonTextColor,
    }),
    buttonTextHover: RX.Styles.createTextStyle({
        color: Colors.buttonTextHover,
    }), 
    titleLogin: RX.Styles.createTextStyle({
        color: "black",
        fontSize:24,
        font:Fonts.displayBold,
        marginTop:14,
      textAlign:'center'
      }), 
      titleForgot: RX.Styles.createTextStyle({
        color: "#456078",
        fontSize:14,
        font:Fonts.displayRegular,
        textAlign:'left'
      }),
      todoText: RX.Styles.createTextStyle({
          font:Fonts.displayBold,
          fontSize: FontSizes.size16,
          alignSelf: 'stretch',
          backgroundColor: 'transparent',
          textAlign:'left'
      }),
      todoText2: RX.Styles.createTextStyle({
          font:Fonts.displayRegular,
          fontSize: FontSizes.size16,
          alignSelf: 'stretch',
          color:'gray',
          backgroundColor: 'transparent',
          textAlign:'left'
      }),
      todoText3: RX.Styles.createTextStyle({
          font:Fonts.displayBold,
          fontSize: FontSizes.size16,
          alignSelf: 'stretch',
          backgroundColor: 'transparent',
          textAlign:'left',
          marginRight:10
      }),
      todoText4: RX.Styles.createTextStyle({
          font:Fonts.displayRegular,
          fontSize: FontSizes.size16,
          alignSelf: 'stretch',
          color:'gray',

          backgroundColor: 'transparent',
          textAlign:'left'
      }),
};

interface Entries {
    img:string;
    imgText:string;
    title:string;
    content:string;
  }
  import ProgressCircle from 'react-native-progress-circle'
import TodosStore from '../stores/TodosStore';


import FoodsMenu from './FoodsMenu';
import  NavContextStore from '../stores/NavContextStore';
  const  {Carousel} = require('reactxp-carousel')
export default class TodoListPanel extends ComponentBase<TodoListPanelProps, TodoListPanelState> {
    protected _buildState(props: TodoListPanelProps, initState: boolean): Partial<TodoListPanelState> | undefined {
        const partialState: Partial<TodoListPanelState> = {
            width:ResponsiveWidthStore.getWidth(),
            isConsumido:true,
            groupFoods:TodosStore.getFoodGroup(),
            group:TodosStore.getGroup(),
            goal1:TodosStore.getGoal1(),
            goal2:TodosStore.getGoal2(),
            activeSlide:0,
            height:ResponsiveWidthStore.getHeight(),
            entries: [{ 
                img: ImageSource.onboard1,
              imgText:'JOINT WORK',
            
              title:'PLATFORM1',
              content:'No more extra edits and deadlines. Understanding the client is not so difficult, but to involve him in the work is to significantly accelerate the approval of the project. ' },
              { img: ImageSource.onboard2,
              imgText:'JOINT WORK2',         
              title:'PLATFORM2',
              content:'No more extra edits and deadlines. Understanding the client is not so difficult, but to involve him in the work is to significantly accelerate the approval of the project.' },
               ]
        };
        
        partialState.todosCe = TodosStore.getTodosCe();
        partialState.todosAl = TodosStore.getTodosAl();
        partialState.todosDe = TodosStore.getTodosDe();
        return partialState;
    }
_renderItem2 = ({item, index}:{item:Entries,index:number}) => {
    return (
        <RX.View>
       {item.title==='PLATFORM1'?
        <RX.View style={{width:this.state.width,justifyContent:'center',alignItems:'center',flexDirection:'row',marginTop:10}}>
    
<RX.View style={{flex:50,justifyContent:'center',alignItems:'center'}}>
       <ProgressCircle
           percent={this.state.goal2?.proteinas*100/this.state.goal1?.proteinas}
           radius={50}
           borderWidth={8}
       
           color={"black"}
           shadowColor={"#999"}
           bgColor={"#fff"}
           containerStyle={{justifyContent:'center',alignItems:'center'}}
           outerCircleStyle={{alignSelf:'center',justifyContent:'center',alignItems:'center'}}
       ><ProgressCircle
       percent={this.state.goal2?.carbohidratos*100/this.state.goal1?.carbohidratos}
       radius={40}
       borderWidth={8}
       color={"red"}
       shadowColor={"#999"}
       bgColor={"#fff"}
       outerCircleStyle={{alignSelf:'center',justifyContent:'center',alignItems:'center'}}
   ><ProgressCircle
   percent={this.state.goal2?.grasas*100/this.state.goal1?.grasas}
   radius={30}
   borderWidth={8}
   color={"blue"}
   shadowColor={"#999"}
   bgColor={"#fff"}
   outerCircleStyle={{alignSelf:'center',justifyContent:'center',alignItems:'center'}}
><ProgressCircle
   percent={this.state.goal2?.fibra*100/this.state.goal1?.fibra}
   radius={20}
   borderWidth={8}
   color={"yellow"}
   shadowColor={"#999"}
   bgColor={"#fff"}
   outerCircleStyle={{alignSelf:'center',justifyContent:'center',alignItems:'center'}}
>
           </ProgressCircle>
           </ProgressCircle>
           </ProgressCircle>
        </ProgressCircle>
        </RX.View>
<RX.View style={{flex:50,justifyContent:'flex-start',alignItems:'flex-start'}}>
           <RX.View style={{justifyContent:'flex-start',alignItems:'flex-start',marginRight:50}}>

           <RX.View style={{justifyContent:'flex-start',marginBottom:5,alignItems:'flex-start',flexDirection:'row'}}> 
          
           <RX.Text style={_styles.todoText3}>{'Proteinas:'}</RX.Text>
           <RX.Text style={_styles.todoText4}>{(this.state.goal1?.proteinas-this.state.goal2?.proteinas).toFixed(2)+' gr'}</RX.Text> 
           </RX.View>  

           <RX.View style={{justifyContent:'flex-start',marginBottom:5,alignItems:'flex-start',flexDirection:'row'}}> 
           <RX.Text style={_styles.todoText3}>{'Carbos:'}</RX.Text>
           <RX.Text style={_styles.todoText4}>{(this.state.goal1?.carbohidratos-this.state.goal2?.carbohidratos).toFixed(2)+' gr'}</RX.Text> 
           
               </RX.View> 

           <RX.View style={{justifyContent:'flex-start',marginBottom:5,alignItems:'flex-start',flexDirection:'row'}}> 
           <RX.Text style={_styles.todoText3}>{'Grasas:'}</RX.Text>
           <RX.Text style={_styles.todoText4}>{(this.state.goal1?.grasas-this.state.goal2?.grasas).toFixed(2)+' gr'}</RX.Text> 
           
            
            </RX.View> 

           <RX.View style={{justifyContent:'flex-start',marginBottom:5,alignItems:'flex-start',flexDirection:'row'}}> 
           <RX.Text style={_styles.todoText3}>{'Fibra:'}</RX.Text>
           <RX.Text style={_styles.todoText4}>{(this.state.goal1?.fibra-this.state.goal2?.fibra).toFixed(2)+' gr'}</RX.Text> 
           
                 </RX.View>

           </RX.View> 
           </RX.View>
       </RX.View>:   
       <RX.View style={{width:this.state.width,marginBottom:10,backgroundColor:'white',justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
        

           <RX.View style={{flex:1,justifyContent:'center',alignItems:'center'}}>

           <RX.View style={{justifyContent:'center',alignItems:'center'}}> 

           <RX.View style={{justifyContent:'flex-start',alignItems:'flex-start'}}> 
           <RX.Text style={_styles.todoText}>{'Consumidos'}</RX.Text>
           <RX.Text style={_styles.todoText2}>{(this.state.goal2?.total).toFixed(2)+' kcal'}</RX.Text> 
           </RX.View>  

           <RX.View style={{justifyContent:'flex-start',marginTop:10,alignItems:'flex-start'}}> 
           <RX.Text style={_styles.todoText}>{'Restan'}</RX.Text>
           <RX.Text style={_styles.todoText2}>{(this.state.goal1.calorias-this.state.goal2?.total).toFixed(2)+' kcal'}</RX.Text> 
           </RX.View>  
           </RX.View> 
           </RX.View>
           <RX.View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
           <ProgressCircle
           percent={this.state.goal2?.total*100/this.state.goal1?.calorias}
           radius={55}
           borderWidth={10}
           color={"#1273EB"}
           shadowColor={"#999"}
           bgColor={"#fff"}
           outerCircleStyle={{alignSelf:'center',justifyContent:'center',alignItems:'center'}}
       >
         <RX.Text style={[_styles.todoText,{textAlign:'center'}]}>{(this.state.goal2?.total*100/this.state.goal1?.calorias).toFixed(2)+' %'}</RX.Text>
        </ProgressCircle>
           </RX.View>
       </RX.View>}
   
       </RX.View>
    );
}
ref = React.createRef<any>();
onPrev = () =>  {
    if (this.ref && this.ref.current) {
      this.ref.current.snapToPrev();
    }
  }
 onNext = () =>  {
  if (this.ref && this.ref.current) {
    this.ref.current.snapToNext();
  }
}

    render() {

        return (
            <RX.ScrollView  style={ _styles.container }>
             
            
<RX.View style={{justifyContent:'center',alignItems:'center',flex:1,marginTop:20}}>
                <Carousel
            lockScrollWhileSnapping={true}
            data={this.state.entries}
            enableMomentum={false}
             ref={this.ref}
            renderItem={this._renderItem2}
            sliderWidth={this.state.width}
            itemWidth={this.state.width}
            containerCustomStyle={[_styles.slider,{}]}
            contentContainerCustomStyle={[_styles.sliderContentContainer,{}]}
            layout={'default'}                 
             onSnapToItem={(index:number) => this.setState({ activeSlide: index }) }
            scrollEnabled={true}
            vertical={false}
            showsHorizontalScrollIndicator={false}
                /> 

          
</RX.View> 
<RX.View style={{marginTop:20,width:this.state.width,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                <RX.View  style={{flex:10}}>
               </RX.View> 
               <RX.View  style={{flex:40}}>
                <RX.Button onPress={this.onNext} style={{borderRadius:40,marginBottom:10,backgroundColor:'black',alignSelf:'center',alignItems:'center',justifyContent:'center',width:this.state.width*0.3, height:25}}>
                 <RX.Text style={_styles.TextBtn}> {'Restante'}</RX.Text> 
                </RX.Button>
               
               </RX.View>  
               <RX.View style={{flex:40}}>
                <RX.Button onPress={this.onPrev} style={{borderRadius:40,marginBottom:10,backgroundColor:'black',alignSelf:'center',alignItems:'center',justifyContent:'center',width:this.state.width*0.3, height:25}}>
                 <RX.Text style={_styles.TextBtn}> {'Consumido'}</RX.Text> 
                </RX.Button>

               
                </RX.View> 
                <RX.View  style={{flex:10}}>
               </RX.View> 
                </RX.View> 
        <RX.View>
                   <RX.View style={{flex:1,marginTop:40}}>
                   
                         <RX.View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                      
              

                  <RX.View style={{backgroundColor:'white',width:this.state.width,flex:1,justifyContent:'center',alignItems:'center',alignSelf:'center'}}>
                  <RX.ScrollView style={{width:this.state.width,flex:1,}}>
              <FoodsMenu  onPress={ this._completeDelete1 } title={this.state.groupFoods[1]} todos={this.state.todosDe}/>
                </RX.ScrollView>
                         </RX.View>
                       </RX.View>
                       </RX.View>
                   </RX.View>

                   <RX.View style={{}}>
                  
                         <RX.View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
               
                  <RX.View style={{backgroundColor:'white',width:this.state.width,flex:1,justifyContent:'center',alignItems:'center',alignSelf:'center'}}>
                  <RX.ScrollView style={{width:this.state.width,flex:1}}>
                  <FoodsMenu     onPress={ this._completeDelete2 }  title={this.state.groupFoods[2]}  todos={this.state.todosAl}/>
        
                </RX.ScrollView>
                        
                         </RX.View>
                       </RX.View>
                       </RX.View>
                       <RX.View style={{}}>
                 
                         <RX.View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                      
                
                  
                  <RX.View style={{backgroundColor:'white',width:this.state.width,flex:1,justifyContent:'center',alignItems:'center',alignSelf:'center'}}>
                  <RX.ScrollView style={{width:this.state.width,flex:1}}>
                  <FoodsMenu onPress={ this._completeDelete3 }  title={this.state.groupFoods[3]}  todos={this.state.todosCe}/>
        
                </RX.ScrollView>
                         </RX.View>
                       </RX.View>
                       </RX.View>
        <RX.Button onPress={this._completeDelete4} style={{borderRadius:100,marginRight:30,marginBottom:20,marginTop:20,backgroundColor:'#1273EB',alignSelf:'flex-end',alignItems:'center',justifyContent:'center',width:this.state.width*0.15, height:this.state.width*0.15}}>
        <RX.Image source={ImageSource.plus} style={{height:20,width:20}}/>
       </RX.Button>

            </RX.ScrollView>
        );
    }
   


//    private _onChangeTextSearch = (newValue: string) => {
 //       const filteredTodoList = this._filterTodoList(this.state.todos, newValue.trim());
//        this.setState({
//            filteredTodoList,
 //           searchString: newValue,
 //       });
//    };

private _completeDelete1() {
  TodosStore.setGroup(1);
}
private _completeDelete2() {
  
  TodosStore.setGroup(2);

}
private _completeDelete3() {
  
  TodosStore.setGroup(3);
}
private _completeDelete4() {
  
  TodosStore.setGroup(1);
  NavContextStore.navigateToTodoList(undefined,false,false,false,false,false,false,false,true)
}
}
