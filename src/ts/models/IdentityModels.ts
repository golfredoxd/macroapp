/**
* IdentityModels.tsx
* Copyright: Microsoft 2018
*
* Type definitions for user identities.
*/

export type UserId = string;

export interface User {
    id: number;
    firstname: string;
    lastname: string;
    username:string;
    email:string;
    profile_pic:string;
    modo:string;
    groups:{
        1:string;
        2:string;
        3:string;
    },
    goals:{
        fibra:string;
        calorias:string;
        grasas:string;
        proteinas:string;
        carbohidratos:string;
        role:number;
    }
}