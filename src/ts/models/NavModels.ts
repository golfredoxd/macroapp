/**
* NavModels.ts
* Copyright: Microsoft 2018
*
* Interface and enumeration definitions used for app navigation.
* A "navigation context" describes a location within the app and
* any associated state that may be expressed within a deep link.
*
* A "view nav context" describes the state of a view.
*
* A "root nav context" describes the nav context for the app's
* root view - the top of the visual hierarchy. Depending on the
* screen size, the root nav context may be stack-based (consisting
* of a stack of individual panels) or composite (in which multiple
* views are displayed side by side).
*/

import * as _ from 'lodash';

export enum NavViewId {
    TodoComposite = 1,
    TodoList,
    NewTodoNews,
    NewTodoWork,
    NewTodoRecent,
    ViewTodo,
    OnBoarding,
    Login,
    Register,
    ForgotPassword,
    Food,
    Profile,
}

//----------------------------------------
// Root nav contexts
//----------------------------------------
export abstract class RootNavContext {
    constructor(public isStackNav: boolean) {
    }

    abstract clone(): RootNavContext;
}

export abstract class CompositeRootNavContext extends RootNavContext {
    constructor(public viewId: NavViewId) {
        super(false);
    }
}

export class StackRootNavContext extends RootNavContext {
    stack: ViewNavContext[];

    constructor() {
        super(true);
        this.stack = [];
    }

    clone(): StackRootNavContext {
        let clone = new StackRootNavContext();
        _.each(this.stack, navContext => {
            clone.stack.push(navContext.clone());
        });
        return clone;
    }
}

export class TodoRootNavContext extends CompositeRootNavContext {
    todoList: TodoListViewNavContext;

    constructor(selectedTodoId?: string, public showNewTodoPanelWork = false,public showOnBoardingPanel = false ,public showLoginPanel = false, public showRegisterPanel = false, public showForgotPasswordPanel = false, public showNewTodoPanelNews = false, public showNewTodoPanelRecent = false,public showFoodPanel =false,public showProfilePanel =false) {
        super(NavViewId.TodoComposite);
        this.todoList = new TodoListViewNavContext(selectedTodoId);
    }

    clone(): TodoRootNavContext {
        return new TodoRootNavContext(this.todoList.selectedTodoId, this.showNewTodoPanelWork,this.showOnBoardingPanel,this.showLoginPanel,this.showRegisterPanel, this.showForgotPasswordPanel, this.showNewTodoPanelNews ,this.showNewTodoPanelRecent, this.showFoodPanel,this.showProfilePanel);
    }
}

//----------------------------------------
// View nav contexts
//----------------------------------------

export abstract class ViewNavContext {
    constructor(public viewId: NavViewId) {
    }

    abstract clone(): ViewNavContext;
}

export class TodoListViewNavContext extends ViewNavContext {
    constructor(public selectedTodoId?: string) {
        super(NavViewId.TodoList);
    }

    clone(): TodoListViewNavContext {
        return new TodoListViewNavContext(this.selectedTodoId);
    }
}




export class ProfileViewNavContext extends ViewNavContext {
    constructor() {
        super(NavViewId.Profile);
    }

    clone(): ProfileViewNavContext {
        return new ProfileViewNavContext();
    }
}
export class FoodViewNavContext extends ViewNavContext {
    constructor() {
        super(NavViewId.Food);
    }

    clone(): OnBoardingViewNavContext {
        return new OnBoardingViewNavContext();
    }
}

export class OnBoardingViewNavContext extends ViewNavContext {
    constructor() {
        super(NavViewId.OnBoarding);
    }

    clone(): OnBoardingViewNavContext {
        return new OnBoardingViewNavContext();
    }
}


export class NewTodoViewNavContextRecent extends ViewNavContext {
    constructor() {
        super(NavViewId.NewTodoRecent);
    }

    clone(): NewTodoViewNavContextRecent {
        return new NewTodoViewNavContextRecent();
    }
}
export class NewTodoViewNavContextNews extends ViewNavContext {
    constructor() {
        super(NavViewId.NewTodoNews);
    }

    clone(): NewTodoViewNavContextNews {
        return new NewTodoViewNavContextNews();
    }
}

export class NewTodoViewNavContextWork extends ViewNavContext {
    constructor() {
        super(NavViewId.NewTodoWork);
    }

    clone(): NewTodoViewNavContextWork {
        return new NewTodoViewNavContextWork();
    }
}


export class RegisterViewNavContext extends ViewNavContext {
    constructor() {
        super(NavViewId.Register);
    }

    clone(): RegisterViewNavContext {
        return new RegisterViewNavContext();
    }
}


export class ForgotPasswordViewNavContext extends ViewNavContext {
    constructor() {
        super(NavViewId.ForgotPassword);
    }

    clone(): ForgotPasswordViewNavContext {
        return new ForgotPasswordViewNavContext();
    }
}
export class LoginViewNavContext extends ViewNavContext {
    constructor() {
        super(NavViewId.Login);
    }

    clone(): LoginViewNavContext {
        return new LoginViewNavContext();
    }
}

export class ViewTodoViewNavContext extends ViewNavContext {
    constructor(public todoId: string) {
        super(NavViewId.ViewTodo);
    }

    clone(): ViewTodoViewNavContext {
        return new ViewTodoViewNavContext(this.todoId);
    }
}
