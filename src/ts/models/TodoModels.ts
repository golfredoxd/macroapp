/**
* TodoModels.tsx
* Copyright: Microsoft 2018
*
* Data models used with Todo sample app.
*/
export interface Todo {    
id:string,
food_pic:string,
nombre:string,
marca:string,
descripcion:string,
unidad:string,
cantidad:number,
fibra:number,
proteinas:number,
carbohidratos:number,
grasas:number,
calorias:number,
sodio:number,
public:number,
comentario:string,
user_id:number,
}

export interface Todo2 {    
  id:number,
  user_id:number,
  food_id:number,
  group_id:number,
  is_goal:string,
  cantidad:number,
  carbohidratos:number,
  proteinas:number,
  grasas:number,
  fibra:number,
  total:number,
  fecha:string,
  comentario:string,
  food:{
    id:number,
    food_pic:string,
    nombre:string,
    marca:string,
    descripcion:string,
    unidad:string,
    cantidad_porcion:number,
    fibra:number,
    proteinas:number,
    carbohidratos:number,
    grasas:number,
    calorias:number,
    sodio:number,
    public:number,
    comentario:string,
    user_id:number,
  }
  }
  
export interface Food {    
  id:number,
  user_id:number,
  food_id:number,
  group_id:number,
  is_goal:number,
  cantidad:number,
  carbohidratos:number,
  proteinas:number,
  grasas:number,
  fibra:number,
  total:number,
  fecha:string,
  food:{
  id:number,
  food_pic:string,
  nombre:string,
  marca:string,
  descripcion:string,
  unidad:string,
  carbohidratos:number,
  proteinas:number,
  grasas:number,
  fibra:number,
  total:number,
  fecha:string,
  
  }
  }
  
export interface Goal1 {    
  fibra:number,
  calorias:number,
  grasas:number,
  proteinas:number,
  carbohidratos:number
  }
  
export interface Goal2 {    
    fibra:number,
    grasas:number,
    proteinas:number,
    total:number,
    carbohidratos:number
    }
    

export interface Group {    
  1:string,
  2:string,
  3:string,
    }
    