/*
* DeepLinkConverter.tsx
* Copyright: Microsoft 2018
*
* Converts between app (deep-link) URLs and navigation contexts.
*/

import * as assert from 'assert';
import * as _ from 'lodash';

import AppConfig from './AppConfig';
import NavActions from '../app/NavActions';
import * as NavModels from '../models/NavModels';

export default class DeepLinkConverter {
    static getUrlFromContext(context: NavModels.RootNavContext): string {
        let url = AppConfig.getFrontendBaseUrl();

        if (context.isStackNav) {
            const stackContext = context as NavModels.StackRootNavContext;
            const topViewContext = stackContext.stack[stackContext.stack.length - 1];

            if (topViewContext instanceof NavModels.TodoListViewNavContext) {
                url += '/todos';
                return url;
            } else if (topViewContext instanceof NavModels.ViewTodoViewNavContext) {
                url += '/todos?selected=' + encodeURIComponent(topViewContext.todoId);
                return url;
            } else if (topViewContext instanceof NavModels.NewTodoViewNavContextWork) {
                url += '/todos?selected=newWork';
                return url;
            } else if (topViewContext instanceof NavModels.NewTodoViewNavContextNews) {
                url += '/todos?selected=newNews';
                return url;
            } else if (topViewContext instanceof NavModels.NewTodoViewNavContextRecent) {
                url += '/todos?selected=newRecent';
                return url;
            } else if (topViewContext instanceof NavModels.OnBoardingViewNavContext) {
                url = '/onBoarding';
                return url;
            } else if (topViewContext instanceof NavModels.RegisterViewNavContext) {
                url = '/Register';
                return url;
            }  else if (topViewContext instanceof NavModels.ForgotPasswordViewNavContext) {
                url = '/ForgotPassword';
                return url;
            } else if (topViewContext instanceof NavModels.LoginViewNavContext) {
                url += '/Login';
                return url;
            }
        } else {
            let compositeContext = context as NavModels.CompositeRootNavContext;
            if (compositeContext instanceof NavModels.TodoRootNavContext) {
                url += '/todos';
                let todoListContext = context as NavModels.TodoRootNavContext;
                if (todoListContext.showNewTodoPanelWork) {
                    url += '?selected=newWork';
                } else if (todoListContext.todoList.selectedTodoId) {
                    url += '?selected=' + encodeURIComponent(todoListContext.todoList.selectedTodoId);
                }
                 if (todoListContext.showNewTodoPanelRecent) {
                    url += '?selected=newRecent';
                } else if (todoListContext.todoList.selectedTodoId) {
                    url += '?selected=' + encodeURIComponent(todoListContext.todoList.selectedTodoId);
                }
                 if (todoListContext.showNewTodoPanelNews) {
                    url += '?selected=newNews';
                } else if (todoListContext.todoList.selectedTodoId) {
                    url += '?selected=' + encodeURIComponent(todoListContext.todoList.selectedTodoId);
                }
                return url;
            } else if (compositeContext instanceof NavModels.OnBoardingViewNavContext) {
                url += '/OnBoarding';
             
                return url;
            }  else if (compositeContext instanceof NavModels.ForgotPasswordViewNavContext) {
                url += '/ForgotPassword';
             
                return url;
            } else if (compositeContext instanceof NavModels.LoginViewNavContext) {
                url += '/Login';
             
                return url;
            } else if (compositeContext instanceof NavModels.RegisterViewNavContext) {
                url += '/Register';
             
                return url;
            }else if (compositeContext instanceof NavModels.FoodViewNavContext) {
                url += '/Food';
             
                return url;
            } else if (compositeContext instanceof NavModels.ProfileViewNavContext) {
                url += '/Profile';
             
                return url;
            }  else {
                // TODO - need to implement
                assert.fail('Unimplemented');
            }
        }

        return '';
    }

    static getContextFromUrl(url: string, isStackNav: boolean): NavModels.RootNavContext | undefined {
        let urlObj = new URL(url);
        if (!urlObj) {
            return undefined;
        }

        let pathElements = _.map(_.split(urlObj.pathname, '/'), elem => decodeURIComponent(elem));
        if (pathElements.length < 2) {
            return undefined;
        }

        switch (pathElements[1]) {
            case 'todos':
                let selectedTodoId: string | undefined;
                let showNewPanelWork = false;
                let showNewPanelRecent = false;
                let showNewPanelNews = false;

                let selectedValue = urlObj.searchParams.get('selected');
                if (selectedValue === 'newWork') {
                    showNewPanelWork = true;
                } else if (selectedValue === 'newRecent') {
                    showNewPanelRecent = true;
                } else if (selectedValue === 'newNews') {
                    showNewPanelNews = true;
                } else if (selectedValue) {
                    selectedTodoId = selectedValue;
                }

                return NavActions.createTodoListContext(isStackNav, selectedTodoId, showNewPanelWork, false, false,false,false,showNewPanelNews,showNewPanelRecent);
             case 'OnBoarding':
                  
    
                    return NavActions.createTodoListContext(isStackNav, undefined, false, true);  
            case 'Food':
                  
    
                    return NavActions.createTodoListContext(isStackNav, undefined, false, false,false,false,false,false,false,true);  
            case 'Profile':
                  
    
                    return NavActions.createTodoListContext(isStackNav, undefined, false, false,false,false,false,false,false,false,true);
            case 'Login':
                  
    
            return NavActions.createTodoListContext(isStackNav, undefined, false, false, true,false); 
            case 'ForgotPassword':
                  
    
                return NavActions.createTodoListContext(isStackNav, undefined, false, false, false,false,true);
        
            case 'Register':
                  
    
            return NavActions.createTodoListContext(isStackNav, undefined, false, false, false,true);
            
            default:
                return undefined;
        }
    }
}
