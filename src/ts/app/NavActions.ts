/*
* NavActions.tsx
* Copyright: Microsoft 2018
*
* Constructs navigation contexts.
*/

import * as NavModels from '../models/NavModels';

export default class NavActions {
    static createTodoListContext(useStackNav: boolean, selectedTodoId?: string, showNewTodoPanelWork = false,showOnBoardingPanel = false, showLoginPanel = false, showRegisterPanel = false, showForgotPasswordPanel = false, showNewTodoPanelNews = false,showNewTodoPanelRecent =false, showFoodPanel=false,showProfilePanel=false) {
        if (useStackNav) {
            let navContext = new NavModels.StackRootNavContext();
            navContext.stack.push(new NavModels.TodoListViewNavContext(selectedTodoId));
            if (showNewTodoPanelWork) {
                navContext.stack.push(new NavModels.NewTodoViewNavContextWork());
            }  if (showNewTodoPanelNews) {
                navContext.stack.push(new NavModels.NewTodoViewNavContextNews());
            }  if (showNewTodoPanelRecent) {
                navContext.stack.push(new NavModels.NewTodoViewNavContextRecent());
            } if (showLoginPanel) {
                navContext.stack.push(new NavModels.LoginViewNavContext());
            }  if (showRegisterPanel) {
                navContext.stack.push(new NavModels.RegisterViewNavContext());
            }  if (showOnBoardingPanel) {
                navContext.stack.push(new NavModels.OnBoardingViewNavContext());
            }  if (showForgotPasswordPanel) {
                navContext.stack.push(new NavModels.ForgotPasswordViewNavContext());
            }if (showFoodPanel) {
                navContext.stack.push(new NavModels.FoodViewNavContext());
            } if (showProfilePanel) {
                navContext.stack.push(new NavModels.ProfileViewNavContext());
            } else if (selectedTodoId !== undefined) {
                navContext.stack.push(new NavModels.ViewTodoViewNavContext(selectedTodoId));
            }
            return navContext;
        } else {
            return new NavModels.TodoRootNavContext(selectedTodoId, showNewTodoPanelWork, showOnBoardingPanel ,showLoginPanel, showRegisterPanel, showForgotPasswordPanel,showNewTodoPanelNews,showNewTodoPanelRecent,showFoodPanel,showProfilePanel);
        }
    }
}
