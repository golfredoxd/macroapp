/**
* index.web.ts
* Copyright: Microsoft 2018
*
* Web implementation of "images" module.
*/

import AppConfig from '../../app/AppConfig';

import { ImageSourceBase } from './Images';

class ImageSource implements ImageSourceBase {
    todoLogo = AppConfig.getImagePath('todo-logo.png');
    todoSmall = AppConfig.getImagePath('todo-small.png');
    onboard1 = AppConfig.getImagePath('onboard1.png');
    onboard2 = AppConfig.getImagePath('onboard2.png');
    onboard3 = AppConfig.getImagePath('onboard3.png');
    onboard4 = AppConfig.getImagePath('onboard4.png');
    menu = AppConfig.getImagePath('menu.png');
    next1 = AppConfig.getImagePath('next1.png');
    next2 = AppConfig.getImagePath('next2.png');
    arrow = AppConfig.getImagePath('arrow.png');
    photo = AppConfig.getImagePath('photo.png');
    react = AppConfig.getImagePath('react.png');
    frame1 = AppConfig.getImagePath('frame1.png');
    frame2 = AppConfig.getImagePath('frame2.png');
    calendar = AppConfig.getImagePath('calendar.png');
    bell = AppConfig.getImagePath('bell.png');
    logo = AppConfig.getImagePath('julio.png');
    dots = AppConfig.getImagePath('dots.png');
    down = AppConfig.getImagePath('down.png');
    plus = AppConfig.getImagePath('plus.png');
    plus2 = AppConfig.getImagePath('plus2.png');
    wait = AppConfig.getImagePath('wait.png');
    off = AppConfig.getImagePath('off.png');
    user = AppConfig.getImagePath('user.png');
    home = AppConfig.getImagePath('home.png');
}

export default new ImageSource();
