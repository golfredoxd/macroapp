/**
* index.native.ts
* Copyright: Microsoft 2018
*
* Native implementation of "images" module.
*/

import { ImageSourceBase } from './Images';

// The React Native bundler handles resource paths at build time, so they need
// to be specified as full string literals (as opposed to being constructed
// programmatically in a helper method).

// We use accessors and "require" calls to defer loading of these images into
// memory until they are actually used. If we were to require them upfront,
// app launch times would increase substantially.
class ImageSource implements ImageSourceBase {
    get todoLogo() { return require('../../../images/todo-logo.png'); }
    get todoSmall() { return require('../../../images/todo-small.png'); }
    get onboard1() { return require('../../../images/onboard1.png'); }
    get onboard2() { return require('../../../images/onboard2.png'); }
    get onboard3() { return require('../../../images/onboard3.png'); }
    get onboard4() { return require('../../../images/onboard4.png'); }
    get menu() { return require('../../../images/menu.png'); }
    get next1() { return require('../../../images/next1.png'); }
    get next2() { return require('../../../images/next2.png'); }
    get arrow() { return require('../../../images/arrow.png'); }
    get photo() { return require('../../../images/photo.png'); }
    get frame1() { return require('../../../images/frame1.png'); }
    get frame2() { return require('../../../images/frame2.png'); }
    get react() { return require('../../../images/react.png'); }
    get bell() { return require('../../../images/bell.png'); }
    get calendar() { return require('../../../images/calendar.png'); }
    get logo() { return require('../../../images/julio.png'); }
    get dots() { return require('../../../images/dots.png'); }
    get down() { return require('../../../images/down.png'); }
    get plus() { return require('../../../images/plus.png'); }
    get wait() { return require('../../../images/wait.png'); }
    get plus2() { return require('../../../images/plus2.png'); }
    get user() { return require('../../../images/user.png'); }
    get home() { return require('../../../images/home.png'); }
    get off() { return require('../../../images/off.png'); }
}

export default new ImageSource();
