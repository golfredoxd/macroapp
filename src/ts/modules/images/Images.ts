/**
* Images.ts
* Copyright: Microsoft 2018
*
* Common interface for "images" module, which handles the fetching
* of all static images.
*/

export interface ImageSourceBase {
    todoLogo: string;
    todoSmall: string;
    onboard1:string;
    onboard2:string;
    onboard3:string;
    onboard4:string;
    menu:string;
    next1:string;
    next2:string;
    arrow:string;
    photo:string;
    react:string;
    frame1:string;
    frame2:string;
    calendar:string;
    bell:string;
    logo:string;
    down:string;
    dots:string;
    plus:string;
    
    plus2:string;
    wait:string;    
    off:string;   
    home:string;   
    user:string;
}
