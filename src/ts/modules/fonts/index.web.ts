/**
* index.web.ts
* Copyright: Microsoft 2018
*
* Web implementation of "fonts" module.
*/

import { FontBase } from './Fonts';

class Fonts implements FontBase {
    monospace = 'monospace';
    displayRobotoBold = '"SF Bold", "Roboto Bold",  sans-serif';
    displayLight = '"SF Semilight", "Montserrat Semilight",  sans-serif';
    displayRegular = '"SF Regular", "Montserrat Regular",  sans-serif';
    displaySemibold = '"SF Semibold", "Montserrat Semibold", sans-serif';
    displayBold = '"SF Bold", "Montserrat Bold",  sans-serif';
}

export default new Fonts();
