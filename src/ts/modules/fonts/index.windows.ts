/**
* index.windows.ts
* Copyright: Microsoft 2018
*
* Windows implementation of "fonts" module.
*/

import { FontBase } from './Fonts';

class Fonts implements FontBase {
    monospace = 'Montserrat';

    displayRobotoBold = 'Roboto';
    displayLight = 'Montserrat';
    displayRegular = 'Montserrat';
    displaySemibold = 'Montserrat';
    displayBold = 'Montserrat';
}

export default new Fonts();
