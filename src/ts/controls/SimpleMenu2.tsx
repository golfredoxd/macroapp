/**
* SimpleMenu.ts
* Copyright: Microsoft 2018
*
* A simple menu with text items. This component is typically
* rendered within an RX.Popup.
*/

import * as _  from 'lodash';
import * as RX from 'reactxp';
import { ComponentBase } from 'resub';

import { Colors, Fonts, FontSizes } from '../app/Styles';
import ResponsiveWidthStore from '../stores/ResponsiveWidthStore';

import { Todo2 } from '../models/TodoModels';
interface TodoListItemInfo extends VirtualListViewItemInfo {
    todo: Todo2;
  }
export interface MenuItem {
    text: string;
    command: string;
    disabled?: boolean;
    checked?: boolean;
}

export interface MenuProps extends RX.CommonProps {
    todos:Todo2[],
    menuButtonStyles?: RX.Types.StyleRuleSet<RX.Types.ButtonStyle>;
    menuTextStyles?: RX.Types.StyleRuleSet<RX.Types.TextStyle>;

    onSelectItem: (command: string) => void;

    focusFirst?: boolean;
}

interface MenuState {
    todos: TodoListItemInfo[];
    hoverCommand?: string;
    focusedIndex: number;
    width:number;
}


import TodoListItem3 from '../views/TodoListItem3';
const _listItemHeight = 60;
import { VirtualListView, VirtualListViewItemInfo } from 'reactxp-virtuallistview';
import { VirtualListCellRenderDetails } from 'reactxp-virtuallistview/dist/VirtualListCell';

const _styles = {
    menuContainer: RX.Styles.createViewStyle({
        backgroundColor: Colors.menuBackground,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: Colors.menuBorder,
    }),
    menuItemContainer: RX.Styles.createButtonStyle({
        minHeight: 100,
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'row',
    }),
    menuItemHover: RX.Styles.createButtonStyle({
        backgroundColor: Colors.menuItemHover,
    }),
    menuItemText: RX.Styles.createTextStyle({
        flex: 1,
        font: Fonts.displayRegular,
        fontSize: FontSizes.size16,
        color: Colors.menuText,
        marginLeft: 16,
        marginRight: 32,
        marginVertical: 4,
    }),
    checkMarkText: RX.Styles.createTextStyle({
        font: Fonts.displayRegular,
        fontSize: FontSizes.size16,
        marginRight: 16,
        color: Colors.menuText,
    }),
    disabledText: RX.Styles.createTextStyle({
        color: Colors.menuTextDisabled,
    }),
    divider: RX.Styles.createViewStyle({
        height: 1,
        marginVertical: 4,
        backgroundColor: Colors.grayEE,
    }),
    listScroll: RX.Styles.createViewStyle({
        flexDirection: 'column',
        alignSelf: 'stretch',
        height:200,
        backgroundColor: Colors.contentBackground,
    }),
};

export default class SimpleMenu extends ComponentBase<MenuProps, MenuState> {
 
    protected _buildState(props: MenuProps, initialBuild: boolean): Partial<MenuState> {
        if (initialBuild) {
            return {
                hoverCommand: undefined,
                focusedIndex: -1,
                width:ResponsiveWidthStore.getWidth(),
                todos:this.props.todos.map((todo, i) => ({
                    key: i.toString(),
                    height: _listItemHeight,
                    template: 'todo1',
                    todo,
                }))
            };
        }
                
        return {};
    }

    render() {
       

        return (
            <RX.View style={ [_styles.menuContainer,{height:150,width:this.state.width}] } >
              <VirtualListView
                    itemList={ this.state.todos }
                    renderItem={ this._renderItem }
                    style={ _styles.listScroll }
                />
            </RX.View>
        );
    }

    private _renderItem = (details: VirtualListCellRenderDetails<TodoListItemInfo>) => {
        const item = details.item;
        return (
            <TodoListItem3
                todo={ item.todo }
                height={ _listItemHeight }
                isSelected={ false}
                onPress={ ()=>{} }
            />
        );
    };
}
